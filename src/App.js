import React from 'react';
import './App.css';
import './admin/css/bootstrap.min.css'
import './admin/style.css'
import './admin/css/responsive.css'
import './admin/css/bootstrap-select.css'
import './admin/css/perfect-scrollbar.css'
import './admin/css/custom.css'



import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom"
import { Home } from './pages/Home'
import { Innovat } from './pages/InnovaT'

import { Login } from './pages/Login'
import { Register } from './pages/Register'
import { Course } from './pages/Course'
import { Module } from './pages/Module';
import { Plan } from './pages/Plan';
import { Felicidades } from './pages/Felicidades';


import { PageNotFound } from './pages/PageNotFound'
import { Admin } from './pages/Admin'
import ProtectedRoute from './ProtectedRoute';
import { Recovery } from './pages/Recovery';

function App() {
  return (
    <Router>
      <Routes>
        <Route caseSensitive={false} path="/" element={<ProtectedRoute step={1} > <Home /></ProtectedRoute>} />
        <Route caseSensitive={false} path="/innovaT" element={<ProtectedRoute step={1} ><Innovat /></ProtectedRoute>} />

        <Route caseSensitive={false} path="/ingreso" element={<ProtectedRoute tep={1} ><Login /></ProtectedRoute>} />
        <Route caseSensitive={false} path="/recuperarClave" element={<ProtectedRoute tep={1}><Recovery /></ProtectedRoute>} />
        <Route caseSensitive={false} path="/registro" element={<ProtectedRoute step={1}><Register /></ProtectedRoute>} />


        <Route caseSensitive={false} path="/curso" element={<ProtectedRoute step={2}><Course /></ProtectedRoute>} />
        <Route caseSensitive={false} path="/modulo" element={<ProtectedRoute step={2}><Module /></ProtectedRoute>} />
        <Route caseSensitive={false} path="/plan" element={<ProtectedRoute step={2}><Plan /></ProtectedRoute>} />
        <Route caseSensitive={false} path="/felicidades" element={<ProtectedRoute step={2}><Felicidades /></ProtectedRoute>} />

        {/*RUTAS DE ADMINISTRADOR */}
        <Route caseSensitive={false} path="/admin" element={<ProtectedRoute step={3}><Admin /></ProtectedRoute>} />

        {/* ***** */}
        <Route path="*" element={<PageNotFound />} />
      </Routes>

    </Router>
  );
}

export default App;