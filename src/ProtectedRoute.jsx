import React from 'react';
import { Navigate, useNavigate } from 'react-router-dom';

import { getAuthenticated, getUser, removeSession } from 'react-session-persist'

function ProtectedRoute({ children, step }) {
	const navigate = useNavigate();
	// console.log('get user ', getUser(), getAuthenticated())
	switch (step) {
		case 1:
			if (getUser) {
				removeSession();
			}
			return children;
		// if (getAuthenticated() && getUser() !== undefined) {
		// 	if (getUser().tutor !== undefined) {
		// 		return children
		// 	} else {
		// 		return <Navigate to={-1} />;
		// 	}
		// } else {
		// 	console.log("aqui tenemos ")
		// 	navigate("/ingreso");
		// 	window.location = "/ingreso"
		// 	return
		// 	return <Navigate to={-1} />;
		// }
		case 2:
			if (getUser() !== undefined) {
				return children
			} else {
				window.location = "/";
				return <Navigate to={-1} />;
			}

		case 3:
			if (getUser()) {
				if (getUser().tutor) {
					return children
				}
				window.location = "/";
				return <Navigate to={-1} />;
			} else {
				window.location = "/";
				return <Navigate to={-1} />;
			}
		default:
			if (getUser() !== undefined) {
				// console.log('retorna nulo')
				return <Navigate to={-1} />;

			} else {
				return children;

			}
	}


}

export default ProtectedRoute;