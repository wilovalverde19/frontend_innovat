import React from 'react'
import loadingGif from '../images/Cargando.gif'
import ReactLoading from "react-loading";
export const Loading = () => {
    return (
        <center className={'loading'}>
            <ReactLoading type={'spin'} color={'#F97944'} />
            <p>{'Cargando..'}</p>
        </center>
    )
}