import React, { useState, useEffect } from 'react';
import { Loading } from '../loading';
import { Services } from '../../constant/services';
export const PetitionsAd = () => {
    const [menu, setMenu] = useState(false);
    const [infoPe, setInfoPe] = useState(1);
    const [openSlide, setOpenSlide] = useState(true);
    const [loading, setLoading] = useState(false);
    const [loading1, setLoading1] = useState(true);

    const [dataPetition, setDataPetition] = useState([]);
    const [observaciones, setObservacion] = useState('');

    const [infoPlus, setInfoPlus] = useState(null);
    const [dataTurors, setDataTurors] = useState(null);
    const [idTuror, setIdTutor] = useState(null)

    const openInfo = (e) => {
        setInfoPlus(e)
    }
    const acceptPetition = async (id) => {
        var data = JSON.stringify({
            "id_peticion": id,
            "id_tutor": idTuror,
            "observacion": observaciones,
        });
        const url = '/admin/aceptarPeticion';
        const responseSession = await Services(data, url);
        if (responseSession) {
            getPetitions();
            setInfoPlus(null);
        }
    }
    const getPetitions = async () => {
        const data = JSON.stringify({
            "id_tutor": 1
        });
        const url = '/admin/obtenerPeticiones';
        setLoading1(true)

        const responseSession = await Services(data, url);
        setLoading1(false)

        setDataTurors(responseSession.data.data.tutores);
        setDataPetition(responseSession.data.data.pendientes)

    }
    useEffect(() => {
        getPetitions();
    }, [])



    return (
        <>
            <div class="row column_title">
                <div class="col-md-12">
                    <div class="page_title">
                        <h2>Peticiones de Registro</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head">
                            <div class="heading1 margin_0">
                                <h2>Lista de pendientes</h2>
                            </div>
                        </div>
                        <button style={{margin:'10px 30px'}} onClick={() => { getPetitions()}} type="button" class="btn  btn-success">
                                                                <i class="fa fa-refresh" >  </i></button>
                        <div class="table_section padding_infor_info">
                            <div class="table-responsive-sm">
                                {!loading1 ?
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Participe</th>
                                                <th>Emprendimiento</th>

                                                <th>Personas a Cargo</th>
                                                <th>Nivel Académico</th>
                                                <th>Tiempo de funcionamiento</th>
                                                <th>Etapa</th>
                                                <th>Ciudad</th>

                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {dataPetition.map(modulo =>
                                                <tr>
                                                    <td>{modulo.persona.id}</td>
                                                    <td>{modulo.persona.nombres + ' ' + modulo.persona.apellidos}</td>
                                                    <td>{modulo.emprendimiento.nombre}</td>

                                                    <td>{modulo.emprendimiento.num_personas}</td>
                                                    <td>{modulo.persona.nivel_academico}</td>
                                                    <td>{modulo.emprendimiento.tiempo}</td>
                                                    <td>{modulo.emprendimiento.etapa}</td>
                                                    <td>{modulo.emprendimiento.ciudad}</td>
                                                    <td><div class="button_block"><button onClick={() => { openInfo(modulo) }} type="button" class="btn cur-p btn-info">Info</button></div></td>
                                                </tr>
                                            )}


                                        </tbody>
                                    </table>
                                    : <Loading />
                                }
                            </div>
                        </div>
                    </div>
                </div>
                {infoPlus ?
                    <div class="col-md-12">
                        <div class="white_shd full margin_bottom_30">
                            <div class="full graph_head">
                                <div class="heading1 margin_0">
                                    <h2>Información de petición</h2>
                                </div>
                            </div>
                            <div class="full inner_elements">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tab_style3">
                                            <div class="tabbar padding_infor_info">
                                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                    <a onClick={() => { setInfoPe(1) }} className={infoPe === 1 ? 'nav-link active show' : 'nav-link '} id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Personal</a>
                                                    <a onClick={() => { setInfoPe(2) }} className={infoPe === 2 ? 'nav-link active show' : 'nav-link '} id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Emprendimiento</a>
                                                    {/* <a onClick={() => { setInfoPe(3) }} className={infoPe === 3 ? 'nav-link active show' : 'nav-link '} id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Redes sociales</a> */}
                                                </div>
                                                <div class="tab-content invoice_blog" id="v-pills-tabContent">
                                                    <div className={`tab-pane fade ${infoPe === 1 && 'show active'} `} id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                        <div style={{ display: 'flex' }}>
                                                            <p> <strong>Identificación:</strong> {infoPlus.persona.identificacion} <br />
                                                                <strong>Nombres y Apellidos:</strong> {infoPlus.persona.nombres + ' ' + infoPlus.persona.apellidos} <br />
                                                                <strong>Género:</strong> {infoPlus.persona.genero} <br />
                                                                <strong>Nivel Académico:</strong> {infoPlus.persona.nivel_academico}

                                                            </p>
                                                            <p>
                                                                <strong>Correo:</strong> {infoPlus.persona.correo} <br />
                                                                <strong>Estado Civil:</strong> {infoPlus.persona.estado_civil}  <br />
                                                                <strong>Celular:</strong> {infoPlus.persona.movil}  <br />
                                                                <strong>Teléfono:</strong> {infoPlus.persona.telefono}
                                                            </p>
                                                            <p>
                                                                <strong>País de Origen:</strong> {infoPlus.persona.pais_origen} <br />
                                                                <strong>País de Residencia:</strong> {infoPlus.persona.pais_residencia} <br />
                                                                <strong>Ciudad de Residencia:</strong> {infoPlus.persona.ciudad_residencia}
                                                            </p>

                                                        </div>

                                                    </div>
                                                    <div className={`tab-pane fade ${infoPe === 2 && 'show active'} `} id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                                        <div style={{ display: 'flex' }}>
                                                            <p>
                                                                <strong>Nombre:</strong> {infoPlus.emprendimiento.nombre} <br />
                                                                <strong>Ciudad:</strong> {infoPlus.emprendimiento.ciudad} <br />
                                                                <strong>Canton:</strong> {infoPlus.emprendimiento.canton} <br />
                                                                <strong>Tiempo de funcionamiento:</strong> {infoPlus.emprendimiento.tiempo} <br />
                                                            </p>
                                                            <p> <strong>Etapa:</strong> {infoPlus.emprendimiento.etapa} <br />
                                                                <strong>Numero de personas a cargo:</strong> {infoPlus.emprendimiento.num_personas} <br />
                                                                <strong>Canton:</strong> {infoPlus.emprendimiento.canton} <br />
                                                            </p>
                                                            <p> <strong>Descripción:</strong> {infoPlus.emprendimiento.descripcion}
                                                            </p>
                                                        </div>

                                                    </div>
                                                    <div className={`tab-pane fade ${infoPe === 3 && 'show active'} `} id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                                        <p>
                                                            <strong>Linkedin:</strong> Viveros Valverde la mejor solución <br />
                                                            <strong>Facebook:</strong> Loja <br />
                                                            <strong>Twiter:</strong> Loja <br />
                                                            <strong>Instagram:</strong> 2 meses <br />
                                                            <strong>Tiktok:</strong> Inicial <br />
                                                        </p>
                                                    </div>
                                                </div>

                                            </div>
                                            <form id='form-accept' onSubmit={(e) => { e.preventDefault(); acceptPetition(infoPlus.peticion) }}>
                                                <div class="text-center " style={{ margin: '20px' }}>
                                                    <select onChange={(e) => { setIdTutor(e.target.value); }} required>
                                                        <option value='' disabled selected>Designar tutor responsable de seguimiento</option>
                                                        {
                                                            dataTurors.map(tutor =>
                                                                <option value={tutor.element.id}>{'Nombre: ' + tutor.persona.nombres + ' ' + tutor.persona.apellidos + ' Área: ' + tutor.element.carrera}</option>
                                                            )
                                                        }
                                                    </select>
                                                </div>
                                                <div class="text-center">
                                                    <label>Observaciones: </label> <br />
                                                    <textarea style={{ width: '60%', padding: "20px" }} type='textarea' placeholder='...' required value={observaciones} onChange={(e) => { setObservacion(e.target.value) }}></textarea>
                                                    <div class=" text-center">
                                                        <div className='row' style={{ width: '30%', margin: 'auto', marginTop: '20px', marginBottom: '20px' }} >
                                                            <div style={{ margin: 'auto' }} class="button_block">
                                                                <button form='form-accept' type="submit" class="btn cur-p btn-outline-success">Aceptar</button>
                                                            </div>
                                                            <div style={{ margin: 'auto' }} class="button_block">
                                                                <button type="button" class="btn cur-p btn-outline-danger">Rechazar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    null

                }


            </div>
        </>
    )
}
