import React, { useState, useEffect } from 'react';
import { Loading } from '../loading';
import { Services } from '../../constant/services';
import { getAuthenticated, getUser, removeSession } from 'react-session-persist';

export const AvanceParticipe = () => {

    const [menu, setMenu] = useState(false);
    const [infoPe, setInfoPe] = useState(1);
    const [openSlide, setOpenSlide] = useState(true);
    const [loading, setLoading] = useState(false);
    const [loading1, setLoading1] = useState(true);

    const [dataPetition, setDataPetition] = useState([]);
    const [observaciones, setObservacion] = useState('');

    const [infoPlus, setInfoPlus] = useState(null);
    const [dataTurors, setDataTurors] = useState(null);
    const [idTuror, setIdTutor] = useState(null)

    const openInfo = (e) => {
        setInfoPlus(e)
    }
    const enviarComentario = async (id) => {
        var data = JSON.stringify({
            "tutor":getUser().usuario,
            "de": getUser().correo,
            "para": infoPlus.persona.correo,
            "observacion": observaciones,
        });
        const url = '/admin/enviarComentario';
        const responseSession = await Services(data, url);
    
        if (responseSession.data.status) {
            alert(responseSession.data.message);
        }else{
            alert("Error al enviar");
        }
        setObservacion('');
        setInfoPlus(null);
        getPetitions();
        return
    }
    const getPetitions = async () => {

        const infoCache = await getUser();
        const data = JSON.stringify({
            "id_tutor": infoCache.infoTutor.id
        });
        const url = '/admin/obtenerAvances';
        setLoading1(true)

        const responseSession = await Services(data, url);
        setLoading1(false)

        setDataPetition(responseSession.data.data.pendientes);



    }
    useEffect(() => {
        getPetitions();
    }, [])



    return (
        <>
            <div class="row column_title">
                <div class="col-md-12">
                    <div class="page_title">
                        <h2>Participes Asignados</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head">
                            <div class="heading1 margin_0">
                                <h2>Lista</h2>
                            </div>
                        </div>
                        <button style={{margin:'10px 30px'}} onClick={() => { getPetitions()}} type="button" class="btn  btn-success">
                                                                <i class="fa fa-refresh" >  </i></button>
                        <div class="table_section padding_infor_info">
                            <div class="table-responsive-sm">
                                {!loading1 ?
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Participe</th>
                                                <th>Emprendimiento</th>

                                                <th>Avance Actual</th>
                                                <th>Plan de negocios</th>

                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {dataPetition.map(modulo =>
                                                <tr>
                                                    <td>{modulo.persona.id}</td>
                                                    <td>{modulo.persona.nombres + ' ' + modulo.persona.apellidos}</td>
                                                    <td>{modulo.emprendimiento.nombre}</td>
                                                    <td> {!modulo.registro.estado ? `En ${modulo.registro.modulo_actual} módulo` : 'Completado'} </td>
                                                    <td> {!modulo.registro.pdf ? `Pendiente` :
                                                        <div >
                                                            <button onClick={() => { window.open(modulo.registro.pdf, '_blank') }} type="button" class="btn  btn-success">
                                                                <i class="fa fa-eye" > Revisar </i></button>
                                                        </div>}
                                                    </td>
                                                    <td><div class="button_block"><button onClick={() => { openInfo(modulo) }} type="button" class="btn cur-p btn-info">Info</button></div></td>
                                                </tr>
                                            )}

                                        </tbody>
                                    </table>
                                    : <Loading />
                                }
                            </div>
                        </div>
                    </div>
                </div>

                {infoPlus ?
                    <>
                       
                        <div className={`modal fade ${infoPlus && 'show'}`} style={{ display: infoPlus && 'block' }} >
                            <div style={{maxWidth:'80%'}} class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Información de registro</h4>
                                        <button type="button" onClick={() => { setInfoPlus(null); setObservacion(''); }} class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-md-12">
                                            <div class="white_shd full margin_bottom_30">
                                                <div class="full inner_elements">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="tab_style3">
                                                                <div class="tabbar padding_infor_info">
                                                                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                                        <a onClick={() => { setInfoPe(1) }} className={infoPe === 1 ? 'nav-link active show' : 'nav-link '} id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Personal</a>
                                                                        <a onClick={() => { setInfoPe(2) }} className={infoPe === 2 ? 'nav-link active show' : 'nav-link '} id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Emprendimiento</a>
                                                                        {/* <a onClick={() => { setInfoPe(3) }} className={infoPe === 3 ? 'nav-link active show' : 'nav-link '} id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Redes sociales</a> */}
                                                                    </div>
                                                                    <div class="tab-content invoice_blog" id="v-pills-tabContent">
                                                                        <div className={`tab-pane fade ${infoPe === 1 && 'show active'} `} id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                                            <div style={{ display: 'flex' }}>
                                                                                <p> <strong>Identificación:</strong> {infoPlus.persona.identificacion} <br />
                                                                                    <strong>Nombres y Apellidos:</strong> {infoPlus.persona.nombres + ' ' + infoPlus.persona.apellidos} <br />
                                                                                    <strong>Género:</strong> {infoPlus.persona.genero} <br />
                                                                                    <strong>Nivel Académico:</strong> {infoPlus.persona.nivel_academico}

                                                                                </p>
                                                                                <p>
                                                                                    <strong>Correo:</strong> {infoPlus.persona.correo} <br />
                                                                                    <strong>Estado Civil:</strong> {infoPlus.persona.estado_civil}  <br />
                                                                                    <strong>Celular:</strong> {infoPlus.persona.movil}  <br />
                                                                                    <strong>Teléfono:</strong> {infoPlus.persona.telefono}
                                                                                </p>
                                                                                <p>
                                                                                    <strong>País de Origen:</strong> {infoPlus.persona.pais_origen} <br />
                                                                                    <strong>País de Residencia:</strong> {infoPlus.persona.pais_residencia} <br />
                                                                                    <strong>Ciudad de Residencia:</strong> {infoPlus.persona.ciudad_residencia}
                                                                                </p>

                                                                            </div>

                                                                        </div>
                                                                        <div className={`tab-pane fade ${infoPe === 2 && 'show active'} `} id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                                                            <div style={{ display: 'flex' }}>
                                                                                <p>
                                                                                    <strong>Nombre:</strong> {infoPlus.emprendimiento.nombre} <br />
                                                                                    <strong>Ciudad:</strong> {infoPlus.emprendimiento.ciudad} <br />
                                                                                    <strong>Canton:</strong> {infoPlus.emprendimiento.canton} <br />
                                                                                    <strong>Tiempo de funcionamiento:</strong> {infoPlus.emprendimiento.tiempo} <br />
                                                                                </p>
                                                                                <p> <strong>Etapa:</strong> {infoPlus.emprendimiento.etapa} <br />
                                                                                    <strong>Numero de personas a cargo:</strong> {infoPlus.emprendimiento.num_personas} <br />
                                                                                    <strong>Canton:</strong> {infoPlus.emprendimiento.canton} <br />
                                                                                </p>
                                                                               
                                                                            </div>
                                                                            <p style={{textAlign:'justify'}}> <strong>Descripción:</strong> {infoPlus.emprendimiento.descripcion}
                                                                                </p>

                                                                        </div>
                                                                        <div className={`tab-pane fade ${infoPe === 3 && 'show active'} `} id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                                                            <p>
                                                                                <strong>Linkedin:</strong> Viveros Valverde la mejor solución <br />
                                                                                <strong>Facebook:</strong> Loja <br />
                                                                                <strong>Twiter:</strong> Loja <br />
                                                                                <strong>Instagram:</strong> 2 meses <br />
                                                                                <strong>Tiktok:</strong> Inicial <br />
                                                                            </p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <form id='form-accept' onSubmit={(e) => { e.preventDefault(); enviarComentario(infoPlus.registro) }}>

                                                                    <div style={{ padding: '35px' }}>
                                                                        <p style={{ textAlign: 'left' }}>Observaciones: </p> <br />
                                                                        <textarea style={{ width: '100%', padding: "10px" }} type='textarea' placeholder='...' required value={observaciones} onChange={(e) => { setObservacion(e.target.value) }}></textarea>
                                                                        <div class=" text-center">
                                                                            <div className='row' style={{ width: '100%', margin: 'auto', marginTop: '20px', marginBottom: '20px' }} >
                                                                                <div style={{ margin: 'auto' }} class="button_block">
                                                                                    <button form='form-accept' type="submit" class="btn cur-p btn-outline-success">Enviar a Correo</button>
                                                                                </div>
                                                                                {/* <div style={{ margin: 'auto' }} class="button_block">
                                                                <button type="button" class="btn cur-p btn-outline-danger">Rechazar</button>
                                                            </div> */}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="modal-footer">
                                        <button type="button" onClick={() => { setInfoPlus(null); setObservacion(''); }} class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                    :
                    null

                }


            </div>
        </>
    )
}
