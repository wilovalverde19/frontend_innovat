import React from 'react';
import { getUser } from 'react-session-persist';

export const NavBarAd = ({ handle }) => {
    return (
        <div className="inner_container">
            <nav id="sidebar" className='ps'>
                <div className="sidebar_blog_1">
                    <div className="sidebar-header">
                        <div className="logo_section">
                            <a href="index.html">  <img className="logo_icon img-responsive rounded-circle" src={'data:image/png;base64,' + getUser().infoTutor.foto} alt="#" />
                            </a>
                        </div>
                    </div>
                    <div className="sidebar_user_info">
                        <div className="icon_setting"></div>
                        <div className="user_profle_side">
                            <div className="user_img">
                                <img class="img-responsive" src={'data:image/png;base64,' + getUser().infoTutor.foto1} alt="#" />
                            </div>
                            <div className="user_info">
                                <h6>{getUser().usuario}</h6>
                                <p><span className="online_animation"></span> Online</p>


                            </div>
                        </div>
                    </div>
                </div>
                <div className="sidebar_blog_2">
                    <h4>General</h4>
                    <ul className="list-unstyled components">
                        {
                            getUser().admin && (
                                <>
                                    <li><a href='#' onClick={() => { handle(1) }}><i className="fa fa-clock-o orange_color"></i> <span>Peticiones</span></a></li>
                                    <li><a href='#' onClick={() => { handle(2) }}><i className="fa fa-users blue1_color"></i> <span>Tutores</span></a></li>
                                    <li><a disabled href='#' onClick={() => { handle(3) }}><i className="fa fa-object-group blue1_color"></i> <span>Curso</span></a></li>
                                </>
                            )
                            
                        }
                         {
                            getUser().usuario != "Administrador" && (
                                <>
                                    <li><a href='#' onClick={() => { handle(4) }}><i className="fa fa-user orange_color"></i> <span>Participes</span></a></li>
                                </>
                            )
                            
                        }


                    </ul>
                </div>
            </nav>
        </div>

    )
}

