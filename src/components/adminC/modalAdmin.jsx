import React, { useState, useEffect } from 'react';
import { Loading } from '../loading';
import axios from 'axios';
import '../../styles/admin.css';
import { Buffer } from 'buffer';
export const ModalAdmin = ({ close, success, data, type }) => {
    const renderSwitchTitle = (value) => {
        //console.debug("cargamos estado general " + status);
        switch (value) {
            case 0:
                return (<h4 class="modal-title">Registro de Módulo</h4>)
            case 1:
                return (<h4 class="modal-title">Eliminar Módulo</h4>)
        }
    }
    const renderSwitchDescription = (value) => {
        //console.debug("cargamos estado general " + status);
        switch (value) {
            case 0:
                return (<h4 class="modal-title">Registro de Módulo</h4>)
            case 1:
                return (<h5>¿ Está seguro que desea eliminar el módulo con nombre: <i>{data.titulo}</i>  y posición:  <i>{data.posicion} </i> ?</h5>)
        }
    }
    return (
        <>
            <div className='modal fade show' style={{ display: 'block' }} >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            {renderSwitchTitle(type)}
                            <button type="button" onClick={() => { close() }} class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="login_form">
                                {renderSwitchDescription(type)}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" onClick={() => { close() }} class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
                            <button type="submit" onClick={() => { success() }} class="btn btn-outline-success" data-dismiss="modal">Continuar</button>


                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}
