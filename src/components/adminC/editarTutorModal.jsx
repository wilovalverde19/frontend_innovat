import React, { useState, useEffect } from 'react';
import { Loading } from '../loading';
import axios from 'axios';
import '../../styles/admin.css';
import { Buffer } from 'buffer';
import { Services } from '../../constant/services';
import $ from 'jquery';

export const EditarTutorModal = ({ close,dataTutor, success }) => {
    const [loading, setLoading] = useState(false);

    const [correo, setCorreo] = useState(dataTutor.credenciales.usuario);
    const [pass, setPass] = useState(dataTutor.credenciales.clave);
    console.log(dataTutor)
    /*Datos de persona */
    const [identificacion, setIdentificacion] = useState(dataTutor.persona.identificacion);
    const [nombres, setNombres] = useState(dataTutor.persona.nombres);
    const [apellidos, setApellidos] = useState(dataTutor.persona.apellidos);
    const [genero, setGenero] = useState('admin');
    const [ciudadResidencia, setCiudadResidencia] = useState(dataTutor.persona.ciudad_residencia);
    const [paisResidencia, setPaisResidencia] = useState(dataTutor.persona.pais_residencia);
    const [paisOrigen, setPaisOrigen] = useState(dataTutor.persona.pais_origen);
    const [movil, setMovil] = useState(dataTutor.persona.identificacion);
    const [telefono, setTelefono] = useState('0');
    const [nivelAcademico, setNivelAcademico] = useState(dataTutor.tutor.tipo);
    const [estadoCivil, setEstadoCivil] = useState('admin');
    /*-----------------*/

    /* Datos de tutor*/
    const [tipoTutor, setTipoTutor] = useState('profesor');
    const [facultad, setFacultad] = useState(dataTutor.tutor.facultad);
    const [carrera, setCarrera] = useState(dataTutor.tutor.carrera);
    const [especialidad, setEspecialidad] = useState(dataTutor.tutor.especialidad);
    const [foto, setFoto] = useState(dataTutor.tutor.foto1);
    /*-----------------*/
    const [notificacion, setNotificacion] = useState(true);
   

    const handleFoto = () => {
        var file = document.querySelector('input[type="file"]').files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            var fileSize = document.querySelector('input[type="file"]').files[0].size;
            var siezekiloByte = parseInt(fileSize / 1024);
            console.log("tenemos tmanio ", siezekiloByte)

            if (siezekiloByte >  500) {
                alert("La imagen debe tener un tamaño maximo de 500kb");
                setFoto(null)
            }else{
                setFoto(reader.result.replace(/^data:image\/[a-z]+;base64,/, ""));
            }
        };

    }
    const setTutor = async () => {
        if(!foto){
            alert("Ingrese una foto válida")
            return;
        }
        setLoading(true);
        let buff = new Buffer.from(foto);
        const data = JSON.stringify({
            "tutor": {
                "id": dataTutor.tutor.id,
                "tipo": tipoTutor,
                "carrera": carrera,
                "foto": buff,
                "foto1": foto,
                "especialidad": especialidad,
                "facultad": facultad
            },
            "persona": {
                "id": dataTutor.persona.id,
                "identificacion": identificacion,
                "nombres": nombres,
                "apellidos": apellidos,
                "genero": genero,
                "correo": correo,
                "tipo_identificacion": 'CED',
                "ciudad_residencia": ciudadResidencia,
                "pais_residencia": paisResidencia,
                "pais_origen": paisOrigen,
                "movil": movil,
                "telefono": telefono,
                "nivel_academico": nivelAcademico,
                "estado_civil": estadoCivil
            },
            "credenciales": {
                "id": dataTutor.credenciales.id,
                "usuario": correo,
                "clave": pass,
                "tipo": "primera"
            },
            "notificar":false
        });

        const url = '/admin/editarTutor';
        const responseSession = await Services(data, url);
        if (responseSession) {
            setLoading(false);
            if (responseSession.data.status) {
                alert("Tutor editado correctamente")
                success();
                close();
            } else {
                alert(responseSession.data.message)
            }
        }
    }



    return (
        <>
            <div className='modal fade show' style={{ display: 'block' }} >
                <div class="modal-dialog" style={{ maxWidth: '50%' }}>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Editar Tutor</h4>
                            <button type="button" onClick={() => { close() }} class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="login_form RegisterTutor">
                                <form id='form-registerTutor' onSubmit={(e) => { e.preventDefault(); setTutor(); }}>
                                    <div className='contentInput'>
                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Datos Personales</strong></p><br />
                                        <div class="field">
                                            <input id='foto' size="5120" onChange={() => { handleFoto() }} type='file' accept="image/png,image/jpeg" required />
                                            <input type='text' onChange={(e) => { setIdentificacion(e.target.value.replace(/\D/g, '')) }} value={identificacion} placeholder='Identificación' required />

                                        </div>
                                        <div class="field">

                                            {/* <select onChange={(e) => { setTipoPa(e.target.value) }} required >
                                                <option value='' disabled selected>Elige Tipo de Participante</option>
                                                <option value={'estudiante'}>Estudiante</option>
                                                <option value={'profesor'}>Profesor</option>
                                            </select> */}
                                        </div>
                                        <div class="field">
                                            <input type='text' onChange={(e) => { setNombres(e.target.value) }} value={nombres} placeholder='Nombres' required />
                                            <input type='text' onChange={(e) => { setApellidos(e.target.value) }} value={apellidos} placeholder='Apellidos' required />

                                        </div>
                                        <div class="field">
                                            {/* <div style={{ width: '47%' }}>
                                                <p style={{ margin: 'auto' }} >Fecha de nacimiento:</p>
                                                <input type='date' onChange={(e) => { setFechaNacimiento(e.target.value) }} value={fechaNacimiento} required />
                                            </div> */}
                                            {/* <select onChange={(e) => { setGenero(e.target.value) }} value={genero} required >
                                                <option value='' disabled selected hidden >Elige Género</option>
                                                <option value={'Masculino'}>Masculino</option>
                                                <option value={'Femenino'}>Femenino</option>
                                            </select> */}
                                        </div>
                                        {/* <div class="field">
                                            <select onChange={(e) => { setEtnia(e.target.value) }} value={etnia} required >
                                                <option value='' disabled selected>Elige Etnia</option>
                                                <option value={'Meztizo'}>Meztizo</option>
                                                <option value={'Afro'}>Afro</option>
                                            </select>
                                            <select onChange={(e) => { setEstadoCivil(e.target.value) }} value={estadoCivil} required >
                                                <option value='' disabled selected>Elige Estado Civil</option>
                                                <option value={'Soltero'}>Soltero</option>
                                                <option value={'Casado'}>Casado</option>
                                            </select>
                                        </div> */}
                                       
                                        <div class="field">
                                            <input type='text' onChange={(e) => { setMovil(e.target.value.replace(/\D/g, '')) }} value={movil} placeholder='Número de Contacto' required />
                                            <input type='text' onChange={(e) => { setCiudadResidencia(e.target.value) }} value={ciudadResidencia} placeholder='Ciudad de Residencia' required />
                                        </div>
                                        <div class="field">
                                            {/* <input type='text' onChange={(e) => { setTelefono(e.target.value.replace(/\D/g, '')) }} value={telefono} placeholder='Teléfono Convencional' required /> */}

                                        </div>
                                        <div class="field">
                                            <select onChange={(e) => { setPaisOrigen(e.target.value) }} value={paisOrigen} required >
                                                <option value='' disabled selected>Elige País de Origen</option>
                                                <option value={'Ecuador'}>Ecuador</option>
                                                <option value={'Colombia'}>Colombia</option>
                                            </select>
                                        </div>
                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Datos Académicos</strong></p><br />
                                        <div class="field">
                                            <select onChange={(e) => { setNivelAcademico(e.target.value) }} value={tipoTutor} required >
                                                <option value='' disabled selected>Nivel Académico</option>
                                                <option value={'Técnico'}>Técnico</option>
                                                <option value={'Superior'}>Superior</option>
                                                <option value={'Posgrado'}>Posgrado</option>
                                                <option value={'Doctorado'}>Doctorado</option>
                                            </select>

                                            <input type='text' onChange={(e) => { setFacultad(e.target.value) }} value={facultad} placeholder='Facultad' required />

                                        </div>
                                        <div class="field">
                                            <input type='text' onChange={(e) => { setCarrera(e.target.value) }} value={carrera} placeholder='Carrera' required />
                                            <input type='text' onChange={(e) => { setEspecialidad(e.target.value) }} value={especialidad} placeholder='Especialidad' required />

                                        </div>


                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Credenciales</strong></p><br />
                                        <div class="field">
                                            <input type='email'  placeholder='Correo' value={correo} onChange={(e) => { setCorreo(e.target.value) }} required />
                                            <input type='text' placeholder='Contraseña' value={pass} onChange={(e) => { setPass(e.target.value) }} required />

                                        </div>


                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" onClick={() => { close() }} class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
                            <button type="submit" form='form-registerTutor' class="btn btn-outline-warning" data-dismiss="modal">Editar</button>


                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}
