import React, { useState, useEffect } from 'react';
import { Loading } from '../loading';
import axios from 'axios';
import '../../styles/admin.css';
import { Buffer } from 'buffer';
export const RegisterModuleModal = ({ close, success }) => {

    const [infoPerfil, setInfoPerfil] = useState(null);
    const [openModalPerfil, setOpenModalPerfil] = useState(false);
    const [loading, setLoading] = useState(false);
    const [dataTurors, setDataTurors] = useState(null);

    const [correo, setCorreo] = useState('');
    const [pass, setPass] = useState('');
    const [tipoPa, setTipoPa] = useState('');
    const [etnia, setEtnia] = useState('');


    /*Datos de persona */
    const [identificacion, setIdentificacion] = useState('');
    const [nombres, setNombres] = useState('');
    const [fechaNacimiento, setFechaNacimiento] = useState('');
    const [apellidos, setApellidos] = useState('');
    const [genero, setGenero] = useState('');
    const [ciudadResidencia, setCiudadResidencia] = useState('');
    const [paisResidencia, setPaisResidencia] = useState('');
    const [paisOrigen, setPaisOrigen] = useState('');
    const [movil, setMovil] = useState('');
    const [telefono, setTelefono] = useState('');
    const [nivelAcademico, setNivelAcademico] = useState('');
    const [estadoCivil, setEstadoCivil] = useState('');
    /*-----------------*/

    /* Datos de tutor*/
    const [tipoTutor, setTipoTutor] = useState('');
    const [facultad, setFacultad] = useState('');
    const [carrera, setCarrera] = useState('');
    const [especialidad, setEspecialidad] = useState('');
    const [foto, setFoto] = useState('');

    /*-----------------*/
    const handleFoto = () => {
        var file = document.querySelector('input[type="file"]').files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            setFoto(reader.result.replace(/^data:image\/[a-z]+;base64,/, ""));
        };
    }
    const setTutor = () => {
        setLoading(true);
        let buff = new Buffer.from(foto);
        var data = JSON.stringify({
            "tutor": {
                "tipo": tipoTutor,
                "carrera": carrera,
                "foto": buff,
                "especialidad": especialidad,
                "facultad": facultad
            },
            "persona": {
                "identificacion": identificacion,
                "nombres": nombres,
                "apellidos": apellidos,
                "genero": genero,
                "correo": correo,
                "tipo_identificacion": 'CED',
                "ciudad_residencia": ciudadResidencia,
                "pais_residencia": paisResidencia,
                "pais_origen": paisOrigen,
                "movil": movil,
                "telefono": telefono,
                "nivel_academico": nivelAcademico,
                "estado_civil": estadoCivil
            },
            "credenciales": {
                "usuario": correo,
                "clave": pass,
                "tipo": "primera"
            }
        });

        var config = {
            method: 'post',
            url: 'http://localhost:4000/admin/registrarTutor',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                setLoading(false);
                if (response.data.status) {
                    success();
                    close();
                } else {
                    alert(response.data.message)
                }
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    useEffect(() => {
        // getPetitions();
    }, [])

    return (
        <>
            <div className='modal fade show' style={{ display: 'block' }} >
                <div className="modal-dialog" style={{ maxWidth: '50%' }}>
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Registro de Tutor</h4>
                            <button type="button" onClick={() => { close() }} className="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div className="modal-body">
                            <div className="login_form RegisterTutor">
                                <form id='form-registerTutor' onSubmit={(e) => { e.preventDefault(); setTutor(); }}>
                                    <div className='contentInput'>
                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Datos Personales</strong></p><br />
                                        <div className="field">
                                            <input id='foto' onChange={() => { handleFoto() }} type='file' accept="image/png,image/jpeg" required />
                                        </div>
                                        <div className="field">
                                            <select onChange={(e) => { setTipoPa(e.target.value) }} required >
                                                <option value='' disabled selected>Elige Tipo de Participante</option>
                                                <option value={'estudiante'}>Estudiante</option>
                                                <option value={'profesor'}>Profesor</option>
                                            </select>
                                            <input type='text' onChange={(e) => { setIdentificacion(e.target.value.replace(/\D/g, '')) }} value={identificacion} placeholder='Identificación' required />

                                        </div>
                                        <div className="field">
                                            <input type='text' onChange={(e) => { setNombres(e.target.value) }} value={nombres} placeholder='Nombres' required />
                                            <input type='text' onChange={(e) => { setApellidos(e.target.value) }} value={apellidos} placeholder='Apellidos' required />

                                        </div>
                                        <div className="field">
                                            <div style={{ width: '47%' }}>
                                                <p style={{ margin: 'auto' }} >Fecha de nacimiento:</p>
                                                <input type='date' onChange={(e) => { setFechaNacimiento(e.target.value) }} value={fechaNacimiento} required />
                                            </div>
                                            <select onChange={(e) => { setGenero(e.target.value) }} value={genero} required >
                                                <option value='' disabled selected hidden >Elige Género</option>
                                                <option value={'Masculino'}>Masculino</option>
                                                <option value={'Femenino'}>Femenino</option>
                                            </select>
                                        </div>
                                        <div className="field">
                                            <select onChange={(e) => { setEtnia(e.target.value) }} value={etnia} required >
                                                <option value='' disabled selected>Elige Etnia</option>
                                                <option value={'Meztizo'}>Meztizo</option>
                                                <option value={'Afro'}>Afro</option>
                                            </select>
                                            <select onChange={(e) => { setEstadoCivil(e.target.value) }} value={estadoCivil} required >
                                                <option value='' disabled selected>Elige Estado Civil</option>
                                                <option value={'Soltero'}>Soltero</option>
                                                <option value={'Casado'}>Casado</option>
                                            </select>
                                        </div>
                                        <div className="field">
                                            <select onChange={(e) => { setPaisOrigen(e.target.value) }} value={paisOrigen} required >
                                                <option value='' disabled selected>Elige País de Origen</option>
                                                <option value={'Ecuador'}>Ecuador</option>
                                                <option value={'Colombia'}>Colombia</option>
                                            </select>
                                            <select onChange={(e) => { setNivelAcademico(e.target.value) }} value={nivelAcademico} required >
                                                <option value='' disabled selected>Elige Nivel Académico</option>
                                                <option value={'Educación Primaria'}>Educación Primaria</option>
                                                <option value={'Educación Secundaria'}>Educación Secundaria</option>
                                            </select>
                                        </div>
                                        <div className="field">

                                            <input type='text' onChange={(e) => { setMovil(e.target.value.replace(/\D/g, '')) }} value={movil} placeholder='Número de Contacto' required />

                                        </div>
                                        <div className="field">
                                            <input type='text' onChange={(e) => { setCiudadResidencia(e.target.value) }} value={ciudadResidencia} placeholder='Ciudad de Residencia' required />
                                            <input type='text' onChange={(e) => { setTelefono(e.target.value.replace(/\D/g, '')) }} value={telefono} placeholder='Teléfono Convencional' required />

                                        </div>
                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Datos Tutor</strong></p><br />
                                        <div className="field">
                                            <select onChange={(e) => { setTipoTutor(e.target.value) }} value={tipoTutor} required >
                                                <option value='' disabled selected>Elige Tipo de Tutor</option>
                                                <option value={'Interno'}>Educación Primaria</option>
                                                <option value={'Externo'}>Educación Secundaria</option>
                                            </select>
                                            <input type='text' onChange={(e) => { setFacultad(e.target.value) }} value={facultad} placeholder='Facultad' required />

                                        </div>
                                        <div className="field">
                                            <input type='text' onChange={(e) => { setCarrera(e.target.value) }} value={carrera} placeholder='Carrera' required />
                                            <input type='text' onChange={(e) => { setEspecialidad(e.target.value) }} value={especialidad} placeholder='Especialidad' required />

                                        </div>


                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Credenciales</strong></p><br />
                                        <div className="field">
                                            <input type='email' placeholder='Correo' value={correo} onChange={(e) => { setCorreo(e.target.value) }} required />
                                            <input type='text' placeholder='Contraseña' value={pass} onChange={(e) => { setPass(e.target.value) }} required />

                                        </div>


                                    </div>

                                </form>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" onClick={() => { close() }} className="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
                            <button type="submit" form='form-registerTutor' className="btn btn-outline-success" data-dismiss="modal">Registrar</button>


                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}
