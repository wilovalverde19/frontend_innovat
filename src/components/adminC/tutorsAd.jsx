import React, { useState, useEffect } from 'react';
import { Loading } from '../loading';
import { RegisterTutorModal } from './registerTutorModal';
import { EditarTutorModal } from './editarTutorModal';

import axios from 'axios';
import { Services } from '../../constant/services';

export const TutorsAd = () => {
    const [infoPerfil, setInfoPerfil] = useState(null);
    const [openModalPerfil, setOpenModalPerfil] = useState(false);
    const [editnModalPerfil, setOpenModalEdit] = useState(false);

    const [loading, setLoading] = useState(false);
    const [dataTurors, setDataTurors] = useState(null);
    const [registerModal, setRegisterModal] = useState(false);

    const getPetitions = async () => {
        const data = JSON.stringify({
            "activado": true
        });
        const url = '/admin/obtenerTutores';
        const responseSession = await Services(data, url);
        if (responseSession) {
            setDataTurors(responseSession.data.data)
            setLoading(true);
        }
    }
    useEffect(() => {
        getPetitions();
    }, [])
    return (
        <>
            <div class="row column_title">
                <div class="col-md-12">
                    <div class="page_title">
                        <h2>Tutores</h2>
                    </div>
                </div>
            </div>
            {registerModal && <RegisterTutorModal success={() => { getPetitions() }} close={() => { setRegisterModal(false); }} />}
            {editnModalPerfil && <EditarTutorModal success={() => { getPetitions() }} dataTutor={infoPerfil} close={() => { setOpenModalEdit(false); }} />}
            <button onClick={() => { setRegisterModal(true); }} type="button" class="btn btn-success btn-xs" style={{marginBottom:'10px'}}>
                <i class="fa fa-user"> </i> Nuevo tutor
            </button>
            <br />
            <div class="row column1">
                <div class="col-md-12">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head">
                            <div class="heading1 margin_0">
                                <h2>Tutores registrados</h2>
                            </div>
                        </div>
                        {loading ?
                            <div class="full price_table padding_infor_info">
                                <div class="row">
                                    {
                                        dataTurors.map(tutor =>
                                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 profile_details margin_bottom_30">
                                                <div style={{height:"100%"}} class="contact_blog">
                                                    <h4 class="brief">{tutor.tutor.carrera}</h4>
                                                    <div class="contact_inner">
                                                        <div class="left">
                                                            <h3>{tutor.persona.nombres + ' ' + tutor.persona.apellidos}</h3>
                                                            <p><strong>Nivel Académico: </strong>{tutor.persona.nivel_academico}</p>
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-envelope-o"></i> : {tutor.persona.correo}</li>
                                                                <li><i class="fa fa-phone"></i> : {tutor.persona.movil}</li>
                                                            </ul>
                                                        </div>
                                                        <div class="right">
                                                            <div class="profile_contacts">
                                                                <img class="img-responsive" src={'data:image/png;base64,' + tutor.tutor.foto1} alt="#" />
                                                            </div>
                                                        </div>
                                                        <div class="bottom_list">
                                                            <div class="text-center">
                                                                <button onClick={() => { setOpenModalPerfil(true); setInfoPerfil(tutor) }} type="button" class="btn btn-primary btn-xs">
                                                                    <i class="fa fa-eye"> </i> 
                                                                </button>
                                                                <button style={{margin:'0px 10px'}} onClick={() => { setOpenModalEdit(true); setInfoPerfil(tutor) }} type="button" class="btn btn-warning btn-xs">
                                                                    <i class="fa fa-edit"> </i>
                                                                </button>
                                                                {/* <button  onClick={() => { setOpenModalPerfil(true); setInfoPerfil(tutor) }} type="button" class="btn btn-success btn-xs">
                                                                    <i class="fa fa-trash"> </i>
                                                                </button> */}
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className={`modal fade ${openModalPerfil && 'show'}`} style={{ display: openModalPerfil && 'block' }} >
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Imformación</h4>
                                                                <button type="button" onClick={() => { setOpenModalPerfil(false) }} class="close" data-dismiss="modal">&times;</button>
                                                            </div>
                                                            {infoPerfil ?
                                                                <div class="modal-body">
                                                                    <div class="white_shd full margin_bottom_30">
                                                                        <div class="full graph_head">
                                                                            <div class="heading1 margin_0">
                                                                                <h2>Perfil de usuario</h2>
                                                                            </div>
                                                                        </div>
                                                                        <div class="full price_table padding_infor_info">
                                                                            <div class="row">
                                                                                <div class="col-lg-12">
                                                                                    <div class="full dis_flex center_text">
                                                                                        <div class="profile_img">
                                                                                            <img width="180" class="rounded-circle" src={'data:image/png;base64,' + infoPerfil.tutor.foto1} alt="#" />
                                                                                        </div>
                                                                                        <div class="profile_contant">
                                                                                            <div class="contact_inner">
                                                                                                <h3>{infoPerfil.persona.nombres + ' ' + infoPerfil.persona.apellidos}</h3>
                                                                                                <p><strong>Área: </strong>{infoPerfil.tutor.carrera}</p>
                                                                                                <ul class="list-unstyled">
                                                                                                    <li><i class="fa fa-user"></i> : {infoPerfil.persona.identificacion}</li>
                                                                                                    <li><i class="fa fa-envelope-o"></i> : {infoPerfil.persona.correo}</li>
                                                                                                    <li><i class="fa fa-phone"></i> : {infoPerfil.persona.movil}</li>
                                                                                                    <li><i class="fa fa-graduation-cap"></i> : {infoPerfil.persona.nivel_academico}</li>
                                                                                                    <li><i class="fa fa-institution"></i> : {infoPerfil.tutor.tipo}</li>
                                                                                                    <li><i class="fa fa-home"></i> : {infoPerfil.persona.ciudad_residencia + ' ' + infoPerfil.persona.pais_residencia}</li>
                                                                                                    <li><i class="fa fa-child"></i> : {infoPerfil.persona.pais_origen}</li>
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                :
                                                                <Loading />
                                                            }



                                                            <div class="modal-footer">
                                                                <button type="button" onClick={() => { setOpenModalPerfil(false) }} class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                        )
                                    }

                                </div>
                            </div>
                            : <Loading />
                        }


                    </div>
                </div>
            </div>

        </>
    )
}
