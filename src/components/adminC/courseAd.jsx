import React, { useState, useEffect } from 'react';
import { Loading } from '../loading';
import axios from 'axios';
import { RegisterModuleModal } from './registerModuleModal';
import '../../styles/admin.css'
import { ModalAdmin } from './modalAdmin';
import { Services } from '../../constant/services';
import { EditCurseModal } from './editCurseModal';

export const CourseAd = () => {
    const [menu, setMenu] = useState(false);
    const [infoPe, setInfoPe] = useState(1);
    const [openSlide, setOpenSlide] = useState(true);
    const [loading, setLoading] = useState(false);
    const [loading1, setLoading1] = useState(false);
    const [modalDelete, setModalDelete] = useState(false);
    const [registerModal, setRegisterModal] = useState(false);
    const [editCurseModal, setEditCurseModal] = useState(false);

    const [dataModules, setDataModules] = useState(null);
    const [infoPlus, setInfoPlus] = useState(null);
    const [dataCourse, setDataCourse] = useState(null);
    const [idTuror, setIdTutor] = useState(null);

    const [dataModal, setDataModal] = useState(null);

    const openInfo = (e) => {
        setInfoPlus(e)
    }
    const deleteModule = async () => {

        const data = JSON.stringify({
            "id_modulo": dataModal.id
        });
        const url = '/admin/eliminarModulo';
        const responseSession = await Services(data, url);
        if (responseSession) {
            alert(responseSession.data.message);
            setModalDelete(false);
            getModuleCourse();
        } else {
            alert('Error de servidor');

        }
    }
    const getModuleCourse = async () => {
        const data = JSON.stringify(
            {
                "id_course": 1
            }
        );

        const url = '/admin/obtenerCursoCompleto';
        const responseSession = await Services(data, url);
        if (responseSession) {
            setLoading1(responseSession.data.status)
            setDataCourse(responseSession.data.curso);
            setDataModules(responseSession.data.datosModulos.modulo)
        }
    }
    useEffect(() => {
        getModuleCourse();
    }, [])



    return (
        <>

            <div class="row column_title">
                <div class="col-md-12">
                    <div class="page_title">
                        <h2>Curso</h2>
                    </div>
                </div>

            </div>
            {modalDelete && <ModalAdmin data={dataModal} type={1} success={() => { deleteModule() }} close={() => { setModalDelete(false); }} />}
            {registerModal && <RegisterModuleModal success={() => { getModuleCourse() }} close={() => { setRegisterModal(false); }} />}
            {editCurseModal && <EditCurseModal dataCourse={dataCourse} success={() => { getModuleCourse() }} close={() => { setEditCurseModal(false); }} />}
            <div class="row">
                <div class="col-md-12">
                    <div class="white_shd full margin_bottom_30">
                        <div class="full graph_head">
                            <div class="heading1 margin_0">
                                <h2>Lista de módulos</h2>
                            </div>
                        </div>
                        {loading1 ?
                            <>
                                <div className='desCourse'>

                                    <h1>Título: {dataCourse.titulo}</h1>
                                    <button title='Editar' style={{ float: "right" }} onClick={() => { setEditCurseModal(true) }} type="button" class="btn cur-p btn-warning"><i alt="holi" class="fa fa-edit" /></button>
                                    <div>
                                        <img src={'data:image/png;base64,' + dataCourse.portada} />
                                        <p>Descripción: {dataCourse.descripcion}</p>
                                    </div>

                                </div>


                            </>
                            : <Loading />}
                        <button onClick={() => { setRegisterModal(true); }} type="button" class="btn btn-success btn-xs">
                            <i class="fa fa-user" /> Nuevo Módulo
                        </button>
                        <div class="table_section padding_infor_info">
                            <div class="table-responsive-sm">
                                {loading1 ?
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Posición</th>
                                                <th>Título del Módulo</th>
                                                <th>Descripción</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {dataModules.map(modulo =>
                                                <tr>
                                                    <td>{modulo.posicion}</td>
                                                    <td>{modulo.titulo}</td>
                                                    <td>{modulo.descripcion}</td>
                                                    <td>
                                                        <div class="button_block">
                                                            <button onClick={() => { openInfo(modulo) }} type="button" class="btn cur-p btn-info"><i class="fa fa-eye" /></button>
                                                            <br />
                                                            <button onClick={() => { setDataModal(modulo); setModalDelete(true); }} type="button" class="btn cur-p btn-danger"><i class="fa fa-trash" /></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )}


                                        </tbody>
                                    </table>
                                    : <Loading />
                                }
                            </div>
                        </div>
                    </div>
                </div>
                {infoPlus ?
                    <div class="col-md-12">
                        <div class="white_shd full margin_bottom_30">
                            <div class="full graph_head">
                                <div class="heading1 margin_0">
                                    <h2>Información de petición</h2>
                                </div>
                            </div>
                            <div class="full inner_elements">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="tab_style3">
                                            <div class="tabbar padding_infor_info">
                                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                    <a onClick={() => { setInfoPe(1) }} className={infoPe === 1 ? 'nav-link active show' : 'nav-link '} id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Personal</a>
                                                    <a onClick={() => { setInfoPe(2) }} className={infoPe === 2 ? 'nav-link active show' : 'nav-link '} id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Emprendimiento</a>
                                                    <a onClick={() => { setInfoPe(3) }} className={infoPe === 3 ? 'nav-link active show' : 'nav-link '} id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Redes sociales</a>
                                                </div>
                                                <div class="tab-content invoice_blog" id="v-pills-tabContent">
                                                    <div className={`tab-pane fade ${infoPe === 1 && 'show active'} `} id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                        <div style={{ display: 'flex' }}>
                                                            <p> <strong>Identificación:</strong> {infoPlus.persona.identificacion} <br />
                                                                <strong>Nombres y Apellidos:</strong> {infoPlus.persona.nombres + ' ' + infoPlus.persona.apellidos} <br />
                                                                <strong>Género:</strong> {infoPlus.persona.genero} <br />
                                                                <strong>Nivel Académico:</strong> {infoPlus.persona.nivel_academico}

                                                            </p>
                                                            <p>
                                                                <strong>Correo:</strong> {infoPlus.persona.correo} <br />
                                                                <strong>Estado Civil:</strong> {infoPlus.persona.estado_civil}  <br />
                                                                <strong>Celular:</strong> {infoPlus.persona.movil}  <br />
                                                                <strong>Teléfono:</strong> {infoPlus.persona.telefono}
                                                            </p>
                                                            <p>
                                                                <strong>País de Origen:</strong> {infoPlus.persona.pais_origen} <br />
                                                                <strong>País de Residencia:</strong> {infoPlus.persona.pais_residencia} <br />
                                                                <strong>Ciudad de Residencia:</strong> {infoPlus.persona.ciudad_residencia}
                                                            </p>

                                                        </div>

                                                    </div>
                                                    <div className={`tab-pane fade ${infoPe === 2 && 'show active'} `} id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                                        <div style={{ display: 'flex' }}>
                                                            <p>
                                                                <strong>Nombre:</strong> {infoPlus.emprendimiento.nombre} <br />
                                                                <strong>Ciudad:</strong> {infoPlus.emprendimiento.ciudad} <br />
                                                                <strong>Canton:</strong> {infoPlus.emprendimiento.canton} <br />
                                                                <strong>Tiempo de funcionamiento:</strong> {infoPlus.emprendimiento.tiempo} <br />
                                                            </p>
                                                            <p> <strong>Etapa:</strong> {infoPlus.emprendimiento.etapa} <br />
                                                                <strong>Numero de personas a cargo:</strong> {infoPlus.emprendimiento.num_personas} <br />
                                                                <strong>Canton:</strong> {infoPlus.emprendimiento.canton} <br />
                                                            </p>
                                                            <p> <strong>Descripción:</strong> {infoPlus.emprendimiento.descripcion}
                                                            </p>
                                                        </div>

                                                    </div>
                                                    <div className={`tab-pane fade ${infoPe === 3 && 'show active'} `} id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                                        <p>
                                                            <strong>Linkedin:</strong> Viveros Valverde la mejor solución <br />
                                                            <strong>Facebook:</strong> Loja <br />
                                                            <strong>Twiter:</strong> Loja <br />
                                                            <strong>Instagram:</strong> 2 meses <br />
                                                            <strong>Tiktok:</strong> Inicial <br />
                                                        </p>
                                                    </div>
                                                </div>

                                            </div>
                                            <form id='form-accept' onSubmit={(e) => { e.preventDefault();/* acceptPetition(infoPlus.peticion)*/ }}>
                                                <div class="text-center">
                                                    <select onChange={(e) => { setIdTutor(e.target.value); }} required>
                                                        <option value='' disabled selected>Designar tutor responsable de seguimiento</option>
                                                        {
                                                            dataCourse.map(tutor =>
                                                                <option value={tutor.element.id}>{'Nombre: ' + tutor.persona.nombres + ' ' + tutor.persona.apellidos + ' Área: ' + tutor.element.carrera}</option>
                                                            )
                                                        }
                                                    </select>
                                                </div>
                                                <div class="text-center">
                                                    <label>Observaciones: </label> <br />
                                                    <textarea style={{ width: '60%' }} type='textarea' placeholder='Feecback' required></textarea>
                                                    <div class=" text-center">
                                                        <div className='row' style={{ width: '30%', margin: 'auto', marginTop: '20px', marginBottom: '20px' }} >
                                                            <div style={{ margin: 'auto' }} class="button_block"><button form='form-accept' type="submit" class="btn cur-p btn-outline-success">Aceptar</button></div>
                                                            <div style={{ margin: 'auto' }} class="button_block"><button type="button" class="btn cur-p btn-outline-danger">Rechazar</button></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    null

                }


            </div>
        </>
    )
}
