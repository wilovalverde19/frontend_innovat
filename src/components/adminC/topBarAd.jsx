import React, { useState, useEffect } from 'react';
import { getUser, removeSession } from 'react-session-persist';
import { useNavigate } from 'react-router-dom';
import Logo from '../../images/Innova_Logo.svg'
import $ from 'jquery';
export const TopBarAd = () => {
    const navigate = useNavigate();
    const [menu, setMenu] = useState(false);
    const [openSlide, setOpenSlide] = useState(true);
    const handleOpen = () => {
        if (!openSlide) {
            $("#sidebar").addClass('active');
            $("#momContent").addClass('activeContent');
            //$("#topbar").css('padding-left', '90px')
        } else {
            $("#sidebar").removeClass('active');
            $("#momContent").removeClass('activeContent');
            $("#topbar").css('padding-left', '280px')
        }
        setOpenSlide(!openSlide);
    }
    useEffect(() => {
        handleOpen();
    }, [])

    return (
        <div id='topbar' class="topbar" >
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="full">
                    <button onClick={() => { handleOpen() }} type="button" id="sidebarCollapse" class="sidebar_toggle"><i class="fa fa-bars"></i></button>
                    <div class="logo_section">
                        <a href="index.html">
                            <img class="img-responsive" src={Logo} alt="#" />

                        </a>
                    </div>
                    <div class="right_topbar">
                        <div class="icon_info">
                            <ul>
                                {/* <li><a href="#"><i class="fa fa-envelope-o"></i><span class="badge">3</span></a></li> */}
                            </ul>
                            <ul class="user_profile_dd">
                                <li>
                                    <a class="dropdown-toggle" onClick={() => { setMenu(!menu) }} data-toggle="dropdown">
                                        <img class="img-responsive rounded-circle" src={'data:image/png;base64,' + getUser().infoTutor.foto1} alt="#" />
                                        <span class="name_user">{getUser().usuario}</span>
                                    </a>
                                    <div id='menuAd' class={menu ? 'dropdown-menu show' : 'dropdown-menu'}>
                                        <a class="dropdown-item" href="profile.html">Mi Perfil</a>
                                        <a class="dropdown-item" href="settings.html">Cambiar Contraseña</a>
                                        <a class="dropdown-item" onClick={() => { removeSession(); navigate('/') }} href="#"><span>Salir</span> <i class="fa fa-sign-out"></i></a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav >
        </div >
    )
}
