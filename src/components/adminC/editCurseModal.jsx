import React, { useState, useEffect } from 'react';
import { Loading } from '../loading';
import axios from 'axios';
import '../../styles/admin.css';
import { Buffer } from 'buffer';
import { Services } from '../../constant/services';

export const EditCurseModal = ({ close, success, dataCourse }) => {

    /*Datos del Curso a editar*/
    const [titulo, setTitulo] = useState('');
    const [descripcion, setDescripcion] = useState('')
    const [portada, setPortada] = useState('');

    const [loading, setLoading] = useState(false);

    /*-----------------*/
    const handleFoto = () => {
        var file = document.querySelector('input[type="file"]').files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            setPortada(reader.result.replace(/^data:image\/[a-z]+;base64,/, ""));
        };
    }
    const editCourse = async () => {
        setLoading(true);
        //let buff = new Buffer.from(portada);
        const data = JSON.stringify({
            "curso": {
                "id": dataCourse.id,
                "titulo": titulo,
                //"portada": buff,
                "descripcion": descripcion
            },
        });

        const url = '/admin/editarCurso';
        const responseSession = await Services(data, url);
        if (responseSession) {
            setLoading(false);
            if (responseSession.data.status) {
                success();
                close();
            } else {
                alert(responseSession.data.message)
            }
        }
    }

    useEffect(() => {
        setTitulo(dataCourse.titulo);
        setDescripcion(dataCourse.descripcion);
        setPortada(dataCourse.portada);

    }, [])

    return (
        <>
            <div className='modal fade show' style={{ display: 'block' }} >
                <div class="modal-dialog" style={{ maxWidth: '50%' }}>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Modificar Curso</h4>
                            <button type="button" onClick={() => { close() }} class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="login_form RegisterTutor EditTutor">
                                <form id='form-registerTutor' onSubmit={(e) => { e.preventDefault(); editCourse(); }}>
                                    <div className='contentInput'>
                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Portada</strong></p><br />
                                        <div class="field">
                                            <input style={{ width: "100%" }} id='foto' onChange={() => { handleFoto() }} type='file' accept="image/png,image/jpeg" />
                                        </div>
                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Título del Curso</strong></p><br />
                                        <div class="field">

                                            <input type='text' onChange={(e) => { setTitulo(e.target.value) }} value={titulo} placeholder='Titulo' required />

                                        </div>
                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Descripción</strong></p><br />
                                        <div class="field">

                                            <textarea style={{ width: "100%" }} type='text' onChange={(e) => { setDescripcion(e.target.value) }} value={descripcion} placeholder='Descripción' required />

                                        </div>
                                        <p style={{ fontWeight: '800' }}>  <strong className='title'>Total de Módulos : {dataCourse.cantidad_modulo}</strong></p><br />

                                    </div>

                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" onClick={() => { close() }} class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
                            <button type="submit" form='form-registerTutor' class="btn btn-outline-success" data-dismiss="modal">Editar</button>


                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}
