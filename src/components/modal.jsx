import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleInfo } from '@fortawesome/free-solid-svg-icons';
import { faCircleXmark } from '@fortawesome/free-solid-svg-icons';
import { faCircleCheck } from '@fortawesome/free-solid-svg-icons';
const info = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        borderColor: '#16E0EF',
        width: '40%',
        height: 'auto',
        borderRadius: '20px',
        boxShadox: '0px 10px 20px #00000029',
        background: 'white'

    },
};
const error = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        borderColor: '#EF2527',
        width: '40%',
        height: 'auto',
        borderRadius: '20px',
        boxShadox: '0px 10px 20px #00000029',
        background: 'white'
    },
};
const success = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        borderColor: '#99B525',
        width: '40%',
        height: 'auto',
        borderRadius: '20px',
        boxShadox: '0px 10px 20px #00000029',
        background: 'white'
    },
};

export const Modals = ({ type = 0, open = false, message = 'mesange', handleclose }) => {
    let subtitle;
    const [modalIsOpen, setIsOpen] = React.useState(true);
    return (
        <div className='modal'>
            <Modal
                id='modal'
                isOpen={open}
                style={type === 0 ? error : type === 1 ? info : success}
            >
                <FontAwesomeIcon
                    icon={type === 0 ? faCircleXmark : type === 1 ? faCircleInfo : faCircleCheck}
                    color={type === 0 ? '#EF2527' : type === 1 ? '#16E0EF' : '#99B525'}
                    fontSize={'5vw'}
                    id={'icon-modal'}
                />
                <div style={{ display: 'flex' }}> <p className='msg' style={{ color: '#000' }}>{message}</p></div>


                <div style={{ display: 'flex' }}>
                    <button onClick={handleclose}>Aceptar</button>
                </div>

            </Modal>
        </div>
    );
}