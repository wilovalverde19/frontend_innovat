import React, { useEffect, useState } from 'react'
import '../styles/navbar.css'
import { getAuthenticated, getUser, removeSession } from 'react-session-persist';
import InnovaLogo from '../images/Innova_Logo.svg';
import UnlLogo from '../images/logoUNL.svg';
import { useNavigate } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHouse } from '@fortawesome/free-solid-svg-icons';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';
import { faRightFromBracket } from '@fortawesome/free-solid-svg-icons';

export const Navbar = ({ back = null }) => {
    const navigate = useNavigate();
    const [session, setSession] = useState(false);
    const [dataUser, setDataUser] = useState(null);
    const [openMenu, setOpenMenu] = useState(false);

    const obtener = async () => {
        setDataUser(await getUser());
        setSession(getUser() !== undefined);
    }
    useEffect(() => {
        obtener();
    }, [])

    return (
        <>
            <div className="NavBar">
                <img className='first' src={InnovaLogo} />
                <img className='second' src={UnlLogo} />
            </div>
            <div className='Navigate'>
                {!session ?
                    <>
                        <p onClick={() => { navigate('/') }}>Inicio</p>
                        <p onClick={() => { navigate('/innovaT') }}>Sobre Innova-T</p>
                        <p onClick={() => { navigate('/ingreso') }}>Inicia Sesión</p>
                        <button onClick={() => { navigate('/registro') }}>Regístrate</button>
                    </>
                    :
                    <>
                        {back &&
                            <div className='Back'>
                                <FontAwesomeIcon onClick={() => { back() }} icon={faAngleLeft} color={'#F86629'} />
                            </div>


                        }
                        <div className='sessionGod'>
                            <FontAwesomeIcon onClick={() => { navigate('/curso') }} icon={faHouse} color={'#F86629'} />
                            <FontAwesomeIcon icon={faUser} color={'#F86629'} />
                            <p className='user'>{dataUser.usuario}</p>
                            <FontAwesomeIcon onClick={() => { setOpenMenu(!openMenu) }} icon={faCaretDown} color={'#F86629'} />
                            {openMenu &&
                                <div className='menu'>
                                    {/* <p onClick={() => { alert('vista en desarrollo') }}><FontAwesomeIcon icon={faUser} color={'#000'} /> Mi cuenta </p> */}
                                    <p onClick={() => { removeSession(); navigate('/ingreso') }}><FontAwesomeIcon icon={faRightFromBracket} color={'#000'} /> Cerrar sesión</p>
                                </div>
                            }

                        </div>
                    </>


                }


            </div>
        </>

    )
}