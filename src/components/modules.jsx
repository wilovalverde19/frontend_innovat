import React, { useState } from 'react';
/* ICONOS DE LIBRERIA*/
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';

import { faEye } from '@fortawesome/free-solid-svg-icons';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { faLock } from '@fortawesome/free-solid-svg-icons';
/**/
export const Modules = ({ data = null, postition = 1 }) => {
    const [number, setNumber] = useState(null);
    return (
        <>
            {data &&

                <div className="Modules" >
                    {data.map(modulo =>
                        <>
                            <div>
                                <FontAwesomeIcon
                                    icon={postition === modulo.posicion ? faEye : postition > modulo.posicion ? faCheck : faLock}
                                    color={postition === modulo.posicion ? '#25A6B5' : postition > modulo.posicion ? '#99B525' : '#707070'}
                                />
                                <p className='position'> <b style={{ margin: 'auto' }} >{modulo.posicion}</b> </p>
                                <p className='title'>{modulo.titulo}</p>
                                <FontAwesomeIcon onClick={() => { number === modulo.id ? setNumber(0) : setNumber(modulo.id) }} icon={number !== modulo.id ? faChevronUp : faChevronDown} color={'#B525AB'} />
                            </div>
                            <div>
                                <p className='description' hidden={modulo.id !== number}>{modulo.descripcion}</p>
                            </div>
                        </>

                    )}


                </div >
            }
        </>
    )
}