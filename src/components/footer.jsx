import React from 'react'

export const Footer = () => {
    return (
        <footer>
            <div>
                <p>2023. Todos los derechos reservados. Universidad Nacional de Loja</p>
            </div>
            <div>
                <p>Políticas de Privacidad | Términos de Uso</p>
            </div>
            <div>
                <p>Campus UNL Edificio Innova-T Coworking
                </p>
            </div>

        </footer>
    )
}