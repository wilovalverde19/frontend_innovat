import { useState, useEffect } from 'react';
import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';
import '../styles/login.css';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Loading } from '../components/loading';
import { Modals } from '../components/modal';
import { saveSession, saveUser } from 'react-session-persist';
import { Services } from '../constant/services';
/* ICONOS DE LIBRERIA*/
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faLock } from '@fortawesome/free-solid-svg-icons';
/**/
export const Recovery = () => {
    const navigate = useNavigate();
    const [user, setUser] = useState('');
    /* Hocks utilizados para el control del componente Modal*/
    const [openModal, setOpenModal] = useState(false);
    const [messageModal, setMessageModal] = useState('Mensaje predefinido');
    const [typeModal, setTypeModal] = useState(0);
    /**/
    const [loading, setLoading] = useState(false);

    const setLogin = async () => {
        var data = JSON.stringify({
            "credenciales": {
                "usuario": user
            }
        });


        var url = '/participe/recuperar';
        setLoading(true)
        const responseSession = await Services(data, url);
        setLoading(false);
        if (responseSession.data.status) {
            setTypeModal(2);
            setOpenModal(true);
            setMessageModal("Credenciales enviadas a su correo electrónico");
        } else {
            setTypeModal(0);
            setOpenModal(true);
            setMessageModal("Usuario no registrado");
        }
    }
    return (
        <div className="App">
            <Navbar />
            <div className='padding'>
                {
                    !loading ?
                        <>
                            <section className="Login">
                                <content>
                                    <p className='title'>Recuperar Contraseña</p>
                                    <form onSubmit={(e) => { e.preventDefault(); setLogin(); }}>
                                        <div>
                                            <p>Ingrese el correo electrónico asociado a su cuenta</p>
                                        </div>
                                        <div>
                                            <FontAwesomeIcon icon={faUser} color={'#2A6D75'} />
                                            <input type='email' placeholder='Correo Electrónico' value={user} onChange={(e) => { setUser(e.target.value) }} required />
                                        </div>
                                        <button type='submit' >Aceptar</button>

                                    </form>
                                </content>
                                <div>
                                </div>
                            </section>
                        </>
                        :
                        <Loading />
                }
            </div>
            <Modals type={typeModal} open={openModal} message={messageModal} handleclose={() => { setOpenModal(false) }} />
            <Footer />
        </div >
    )
}