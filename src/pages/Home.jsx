import React from 'react';
import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';
import Logo1 from '../images/ImagenCwk.svg';
import Logo2 from '../images/FotoEvento1.svg';
import Logo3 from '../images/FotoEvento2.svg';
import Logo4 from '../images/FotoEvento3.svg';

import Logo5 from '../images/RosaFlores.svg';
import Logo6 from '../images/Omar_Vivar.svg';
import Logo7 from '../images/Valeria_Ortega.svg';




import '../styles/home.css';

export const Home = () => {
    return (
        <div className="App Home">
            <Navbar />
            <section className="one">
                <div className='text'>
                    <div>
                        <p className='titulo'>
                            Programa de Capacitación Empresarial

                        </p>
                        <p className='textp'>Se parte de nuestro programa de capacitación empresarial y recibe
                            formación personalizada con mentores especializados para llevar tu
                            emprendimiento a otro nivel.</p>
                    </div>
                </div>
                <div className='img'>
                    <img src={Logo1} />
                </div>
            </section>
            <section className="two">
                <p className='title'>Innova-T Coworking</p>
                <p className='text'>Somos un centro de negocios y emprendimientos fundado en 2019 por la Universidad Nacional de Loja. Pretende fortalecer las habilidades, capacidades y destrezas de los empresarios con la finalidad de generar negocios competitivos y sustentables; y, concientizar tanto al futuro empresario como al actual, sobre la importancia de generar responsabilidad social empresarial.</p>
            </section>
            <section className="three">
                <p className='title'>Casos de éxito</p>
                <div>
                    <img src={Logo2} />
                    <img src={Logo3} />
                    <img src={Logo4} />
                </div>
            </section>
            <section className="four">
                <p className='title'>Innovación</p>
                <div className='content'>
                    <p className='text'>Fortalecer las habilidades, capacidades y destrezas de los empresarios con la finalidad de generar negocios competitivos y sustentables; y, concientizar tanto al futuro empresario como al actual, sobre la importancia de generar responsabilidad social empresarial.</p>
                    <div>
                        <img src={Logo4} />
                    </div>

                </div>
            </section>
            <section className="five">
                <p className='title'>Nuestro Equipo</p>
                <div className='content'>
                    <div className='target'>
                        <img src={Logo5} />
                        <p className='title'>Rosa Flores</p>
                        <p className='subtitle'>Ing. Comercio Exterior e Integración</p>
                    </div>
                    <div className='target'>
                        <img src={Logo6} />
                        <p className='title'>Andrea Viteri</p>
                        <p className='subtitle'>Ing. Comercial</p>
                    </div>
                    <div className='target'>
                        <img src={Logo7} />
                        <p className='title'>Andrea Viteri</p>
                        <p className='subtitle'>Ing. Comercial</p>
                    </div>

                </div>
            </section>
            <Footer />
        </div>
    )
}