import React, { useState } from 'react';
import { NavBarAd } from '../components/adminC/navBarAd';
import { TopBarAd } from '../components/adminC/topBarAd';
import { PetitionsAd } from '../components/adminC/petitionsAd';
import { TutorsAd } from '../components/adminC/tutorsAd';
import { CourseAd } from '../components/adminC/courseAd';
import { AvanceParticipe } from '../components/adminC/avanceParticipe';
import { useEffect } from 'react';
export const Admin = () => {
    const [statePage, setStatePage] = useState(0)
    const handleChangeState = (e) => {
        setStatePage(e)
    }
    const renderSwitch = (value) => {
        switch (value) {
            case 1:
                return (<PetitionsAd />)
            case 2:
                return (<TutorsAd />)
            case 3:
                return (<CourseAd />)
            case 4 :
                return (<AvanceParticipe />)
            default:
                return (<><h1 style={{margin:"30px"}}>Bienvenido al panel Administrativo de Innova-T</h1></>)
        }
    }

    return (
        <div className="App">
            <body className="dashboard dashboard_1">
                <div id='momContent' className="full_container">
                    <NavBarAd handle={handleChangeState} />
                    <div id="content">
                        <TopBarAd />
                        <div class="midde_cont">
                            <div class="container-fluid">
                                {renderSwitch(statePage)}
                            </div>
                        </div>
                    </div>
                </div>
            </body>

        </div >
    )
}