import React from 'react';
import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

import Logo1 from '../images/ImagenQueEsInnovaT.svg';
import Logo2 from '../images/ImagenMision.svg';
import Logo3 from '../images/ImagenVision.svg';





import '../styles/innova.css';

export const Innovat = () => {
    return (
        <div className="App Innova">
            <Navbar />
            <section className="one">
            </section>
            <section className="two">
                <div className='content'>
                    <p className='title'>¿QUÉ ES?</p>
                    <div>
                        <fila></fila>
                        <p className='text'>El Coworking Innova-T, es un espacio de trabajo colaborativo e interdisciplinario, vinculante entre estudiantes, docentes, y el sector productivo para desarrollar la innovación, creatividad, marketing, comunicación y producción en la Región Sur del Ecuador.</p>
                    </div>
                </div>
                <div className='image'>
                    <img src={Logo1} />
                </div>
            </section>
            <section className="three">
                <div className='image'>
                    <img src={Logo2} />
                </div>
                <div className='content'>
                    <p className='title'>MISIÓN</p>
                    <div>
                        <fila></fila>
                        <p className='text'>
                            Generar un entorno colaborativo y de transferencia de conocimiento a través de la gestión eficiente de procesos productivos, empresariales y de innovación para el desarrollo de la sociedad.
                        </p>
                    </div>
                </div>
            </section>
            <section className="four">

                <div className='content'>
                    <p className='title'>VISIÓN</p>
                    <div>
                        <fila></fila>
                        <p className='text'>
                            Ser un centro de apoyo empresarial vinculado al entorno productivo que permita dar respuesta a las diferentes necesidades económicas y sociales de la población, minimizando la brecha existente entre la academia, empresa y gobierno.
                        </p>
                    </div>
                </div>
                <div className='image'>
                    <img src={Logo3} />
                </div>
            </section>
            <section className="five">
                <div className='content'>
                    <p className='title'>OBJETIVOS ESTRATÉGICOS:</p>
                    <div>
                        <fila></fila>
                        <div className='estra'>
                            <p className='text'>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    color={'#25A6B5'} />

                                Promover el emprendimiento como eje transversal en las diferentes facultades de la Universidad Nacional de Loja.
                            </p>
                            <p className='text'>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    color={'#25A6B5'} />

                                Promover una cultura innovadora empresarial en la comunidad universitaria.
                            </p>
                            <p className='text'>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    color={'#25A6B5'} />

                                Transformar el pensamiento colectivo y disruptivo para generar innovación.
                            </p>
                            <p className='text'>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    color={'#25A6B5'} />

                                Generar proyectos vinculantes con los actores de ecosistemas empresariales.
                            </p>
                            <p className='text'>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    color={'#25A6B5'} />

                                Generar trabajos de investigación que aporten al desarrollo de las empresas
                            </p>
                            <p className='text'>
                                <FontAwesomeIcon
                                    icon={faCheck}
                                    color={'#25A6B5'} />

                                Construir un ecosistema colaborativo académico – social y productivo entre docentes, estudiantes y empresarios con visión institucional y de servicio en diferentes áreas del conocimiento.
                            </p>
                        </div>

                    </div>
                </div>
            </section>

            <Footer />
        </div>
    )
}