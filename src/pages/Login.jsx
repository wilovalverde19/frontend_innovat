import { useEffect, useState } from 'react';
import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Loading } from '../components/loading';
import { Modals } from '../components/modal';
import { saveSession, saveUser } from 'react-session-persist';
import { Services } from '../constant/services';
/* ICONOS DE LIBRERIA*/
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faKey } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faLock } from '@fortawesome/free-solid-svg-icons';



/**/


import '../styles/login.css';

export const Login = () => {
    const navigate = useNavigate();
    const [user, setUser] = useState('');
    const [pass, setPass] = useState('');
    /* Hocks utilizados para el control del componente Modal*/
    const [openModal, setOpenModal] = useState(false);
    const [messageModal, setMessageModal] = useState('Mensaje predefinido');
    const [typeModal, setTypeModal] = useState(0);
    /**/
    const [loading, setLoading] = useState(false);

    const setLogin = async () => {
        setLoading(true)
        var data = JSON.stringify({
            "credenciales": {
                "usuario": user,
                "clave": pass
            }
        });
        var url = '/participe/login';
        const responseSession = await Services(data, url);
        // console.log("tenemos sesion ", responseSession)
        setLoading(false);

        if (responseSession.data.status) {
            await saveSession(true);
            await saveUser(responseSession.data);
            if (responseSession.data.tutor) {
                navigate('/admin');
            } else {
                navigate('/curso');
            }
        } else {
            setTypeModal(0);
            setOpenModal(true);
            setMessageModal(responseSession.data.message);
        }
    }
    const encripta = async () => {
        var url = '/participe/encriptado';
        const responseSession = await Services({}, url);
        // console.log("aqui encriptacion", responseSession)
    }
    useEffect(() => {

        encripta();
    }, [])
    return (
        <div className="App">

            <Navbar />
            <div className='padding'>
                {!loading ?
                    <>
                        <section className="Login">
                            <content>
                                <p className='title'> <FontAwesomeIcon icon={faKey} color={'#2A6D75'} />Iniciar Sesión</p>
                                <form onSubmit={(e) => { e.preventDefault(); setLogin() }}>
                                    <div>
                                        <FontAwesomeIcon icon={faUser} color={'#2A6D75'} />
                                        <input type='email' placeholder='Correo Electrónico' value={user} onChange={(e) => { setUser(e.target.value) }} required />
                                    </div>
                                    <div>
                                        <FontAwesomeIcon icon={faLock} color={'#2A6D75'} />
                                        <input type='password' placeholder='Contraseña' value={pass} onChange={(e) => { setPass(e.target.value) }} required />
                                    </div>
                                    <a href='#' onClick={() => { navigate('/recuperarClave'); }}> Olvidé mi contraseña </a> <br />
                                    <button type='submit' >Ingresar</button>
                                </form>
                            </content>
                        </section>
                    </>
                    :
                    <Loading />

                }




            </div>

            <Modals type={typeModal} open={openModal} message={messageModal} handleclose={() => { setOpenModal(false) }} />

            <Footer />
        </div >
    )
}