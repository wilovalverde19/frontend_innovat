import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';
import { Modules } from '../components/modules';
import { Loading } from '../components/loading';
import { Modals } from '../components/modal';
import { getUser } from 'react-session-persist';
import { Services } from '../constant/services';

import '../styles/course.css';
export const Course = () => {
    const navigate = useNavigate();

    const [infoCourse, setInfoCourse] = useState({});
    const [loading, setLoading] = useState(false);
    const setLogin = async () => {
        const data = JSON.stringify({
            "usuario": {
                "id_registro": getUser().registro
            }
        });
        const url = '/participe/obtenerCurso';
        const responseSession = await Services(data, url);
        if (responseSession.data.status) {
            if (responseSession.data.complete) {
                navigate('/plan')
                setLoading(responseSession.status);

            } else {
                setInfoCourse(responseSession.data.data);
                setLoading(responseSession.status);
            }
        } else {
            alert('error')
        }
    }
    useEffect(() => {
        setLogin()
    }, [])

    return (
        <div className="App">
            <Navbar />
            {loading ?

                <section className="Course">
                    <Modals />
                    <div className='infoTutor'>
                        <div>
                            <p className='title'>{infoCourse.titulo}</p>
                            <p className='description'>{infoCourse.descripcion}</p>
                        </div>
                        <div className='tutor-content'>
                            <p className='title'>Tutor Asignado</p>
                            <img src={'data:image/png;base64,' + infoCourse.tutor.foto} />
                            <p className='name'>{infoCourse.tutor.nombre} </p>
                            <p className='run'>{infoCourse.tutor.carrera} </p>
                        </div>
                    </div>
                    <div className='content-module'>
                        <button className='orangeBtn' onClick={() => { navigate('/modulo') }}>Empezar</button>
                        <div>
                            <Modules data={infoCourse.modulos} postition={infoCourse.moduloActual} />
                        </div>
                        <div className='portada'>
                            <img src={'data:image/png;base64,' + infoCourse.portada} />
                        </div>
                    </div>
                </section>

                :

                <Loading />

            }

            <Footer />
        </div >
    )
}