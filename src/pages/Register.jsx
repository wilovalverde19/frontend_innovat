import { useState, useEffect } from 'react';
import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressCard } from '@fortawesome/free-solid-svg-icons';
import { faPersonDigging } from '@fortawesome/free-solid-svg-icons';
import '../styles/register.css';
import axios from 'axios';
import { Modals } from '../components/modal';
import { useNavigate } from 'react-router-dom';
import { Services } from '../constant/services';
import { Loading } from '../components/loading';


export const Register = () => {
    const navigate = useNavigate();

    const [loading, setLoading] = useState(false);
    /* Hocks utilizados para el control del componente Modal*/
    const [openModal, setOpenModal] = useState(false);
    const [requerido, setRequerido] = useState(true);

    const [messageModal, setMessageModal] = useState('mensajito');
    const [typeModal, setTypeModal] = useState(0);

    const [correo, setCorreo] = useState('');
    const [pass, setPass] = useState('');
    const [tipoPa, setTipoPa] = useState('');
    const [etnia, setEtnia] = useState('');

    /*Datos de persona */
    const [identificacion, setIdentificacion] = useState('');
    const [nombres, setNombres] = useState('');
    const [apellidos, setApellidos] = useState('');
    const [genero, setGenero] = useState('');
    const [ciudadResidencia, setCiudadResidencia] = useState('');
    const [paisResidencia, setPaisResidencia] = useState('');
    const [paisOrigen, setPaisOrigen] = useState('');
    const [movil, setMovil] = useState('');
    const [telefono, setTelefono] = useState('');
    const [nivelAcademico, setNivelAcademico] = useState(' ');
    const [estadoCivil, setEstadoCivil] = useState('');
    const [fechaNacimiento, setFechaNacimiento] = useState(null);
    /*-----------------*/

    /*Datos de emprendimiento */
    const [nombreEm, setNombreEm] = useState('');
    const [descripcionEm, setDescripcionEm] = useState('');
    const [ciudaEm, setCiudaEm] = useState('');
    const [cantonEm, setCantonEm] = useState('');
    const [etapaEm, setEtapaEm] = useState('');
    const [tiempoEm, setTiempoEm] = useState('');
    const [numPersonasEm, setNumPersonasEm] = useState('');
    /*-----------------*/

    const handleCloseModal = () => {
        if (typeModal === 1) {
            setOpenModal(false);
            navigate('/');
        } else {
            setOpenModal(false);
        }
    }

    const setRegister = async () => {

        const data = {
            "participe": {
                "tipo": tipoPa,
                "etnia": etnia
            },
            "persona": {
                "identificacion": identificacion,
                "tipo_identificacion": "CED",
                "nombres": nombres,
                "apellidos": apellidos,
                "genero": genero,
                "correo": correo,
                "ciudad_residencia": ciudadResidencia,
                "pais_residencia": paisResidencia,
                "pais_origen": paisOrigen,
                "movil": parseInt(movil),
                "telefono": parseInt(telefono),
                "nivel_academico": nivelAcademico,
                "estado_civil": estadoCivil,
                "fecha_nacimiento": fechaNacimiento
            },
            "emprendimiento": {
                "nombre": nombreEm,
                "descripcion": descripcionEm,
                "ciudad": ciudaEm,
                "canton": cantonEm,
                "etapa": etapaEm,
                "tiempo": tiempoEm,
                "num_personas": parseInt(numPersonasEm),
                "estado": false
            },
            "credenciales": {
                "usuario": correo,
                "clave": pass,
                "tipo": "primera"
            }
        };

        const url = '/participe/enviarRegistro';

        setLoading(true);
        const responseSession = await Services(JSON.stringify(data), url);
        setLoading(false)

        if (responseSession.data.status) {
            setTypeModal(1)
            setOpenModal(true);
            setMessageModal('Su registro se ha realizado con éxito. Su cuenta está en etapa de revisión, mantengase pendiente al correo registrado');


        } else {
            setTypeModal(0)
            setOpenModal(true);
            setMessageModal(responseSession.data.message)
        }
    }

    function permite(elEvento, permitidos) {
        // Variables que definen los caracteres permitidos
        var numeros = "0123456789";
        var caracteres = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
        var numeros_caracteres = numeros + caracteres;
        var teclas_especiales = [8, 37, 39, 46];
        // 8 = BackSpace, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha


        // Seleccionar los caracteres a partir del parámetro de la función
        switch (permitidos) {
            case 'num':
                permitidos = numeros;
                break;
            case 'car':
                permitidos = caracteres;
                break;
            case 'num_car':
                permitidos = numeros_caracteres;
                break;
        }

        // Obtener la tecla pulsada 
        var evento = elEvento || window.event;
        var codigoCaracter = evento.charCode || evento.keyCode;
        var caracter = String.fromCharCode(codigoCaracter);

        // Comprobar si la tecla pulsada es alguna de las teclas especiales
        // (teclas de borrado y flechas horizontales)
        var tecla_especial = false;
        for (var i in teclas_especiales) {
            if (codigoCaracter == teclas_especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        // Comprobar si la tecla pulsada se encuentra en los caracteres permitidos
        // o si es una tecla especial
        return permitidos.indexOf(caracter) != -1 || tecla_especial;
    }

    return (
        <div className="App">
            <Navbar />
            {!loading ?
                <>
                    <section className="Registro">
                        <div className='Register-Contents'>
                            <div className='notice'>
                                <p className='title'>! IMPORTANTE ¡</p>
                                <p className='description'>
                                    Luego de realizar su registro, su información será revisada por el
                                    administrador de Innova-T, una vez aceptada la creación de su
                                    cuenta, <b>se le notificará a su correo electrónico</b>

                                </p>
                            </div>
                            <form id='form-register' onSubmit={(e) => { e.preventDefault(); setRegister() }}>
                                <div className='Content-register'>
                                    <p> <FontAwesomeIcon icon={faAddressCard} color={'#2A6D75'} /> <strong className='title'>Datos Personales</strong></p><br />
                                    <div>
                                        <select onChange={(e) => { setTipoPa(e.target.value) }} value={tipoPa} required={requerido} >
                                            <option value='' disabled selected hidden>Elige Tipo de Participante</option>
                                            <option value={'estudiante'}>Estudiante</option>
                                            <option value={'profesor'}>Profesor</option>
                                        </select>
                                        <input type='text' onChange={(e) => { setIdentificacion(e.target.value.replace(/\D/g, '')) }} maxLength={10} minLength={10} value={identificacion} placeholder='Identificación' required={requerido} />
                                    </div>
                                    <div>
                                        <input type='text' onChange={(e) => { setNombres(e.target.value) }} value={nombres} placeholder='Nombres' required={requerido} />
                                        <input type='text' onChange={(e) => { setApellidos(e.target.value) }} value={apellidos} placeholder='Apellidos' required={requerido} />
                                    </div>
                                    <div>
                                        <div style={{ width: '47%' }}>
                                            <p style={{ margin: 'auto' }} >Fecha de nacimiento:</p>
                                            <input type='date' onChange={(e) => { setFechaNacimiento(e.target.value) }} value={fechaNacimiento} required={requerido} />
                                        </div>
                                        <select onChange={(e) => { setGenero(e.target.value) }} value={genero} required={requerido} >
                                            <option value='' disabled selected hidden >Elige Género</option>
                                            <option value={'Masculino'}>Masculino</option>
                                            <option value={'Femenino'}>Femenino</option>
                                        </select>

                                    </div>
                                    <div>

                                        <select onChange={(e) => { setEtnia(e.target.value) }} value={etnia} required={requerido} >
                                            <option value='' disabled selected>Elige Etnia</option>
                                            <option value={'Meztizo'}>Meztizo</option>
                                            <option value={'Afroecuatoiano'}>Afroecuatoiano</option>
                                            <option value={'Indígena'}>Indígena</option>
                                            
                                        </select>
                                        <select onChange={(e) => { setEstadoCivil(e.target.value) }} value={estadoCivil} required={requerido} >
                                            <option value='' disabled selected>Elige Estado Civil</option>
                                            <option value={'Soltero/a'}>Soltero/a</option>
                                            <option value={'Casado/a'}>Casado/a</option>
                                            <option value={'Divorciado/a'}>Divorciado/a</option>
                                            <option value={'Unión Libre'}>Unión Libre</option>


                                        </select>
                                    </div>
                                    <div>
                                        <select onChange={(e) => { setPaisOrigen(e.target.value) }} value={paisOrigen} required={requerido} >
                                            <option value='' disabled selected>Elige País de Origen</option>
                                            <option value={'Argentina'}>Argentina</option>
                                            <option value={'Bolivia'}>Bolivia</option>
                                            <option value={'Chile'}>Chile</option>
                                            <option value={'Colombia'}>Colombia</option>
                                            <option value={'Ecuador'}>Ecuador</option>
                                            <option value={'Paraguay'}>Paraguay</option>
                                            <option value={'Perú'}>Perú</option>
                                            <option value={'Uruguay'}>Uruguay</option>
                                            <option value={'Venezuela'}>Venezuela</option>
                                        </select>
                                        <select onChange={(e) => { setNivelAcademico(e.target.value) }} value={nivelAcademico} required={requerido} >
                                            <option value='' disabled selected>Elige Nivel Académico</option>
                                            <option value={'Educación Primaria'}>Educación Primaria</option>
                                            <option value={'Educación Secundaria'}>Educación Secundaria</option>
                                            <option value={'Bachillerato'}>Bachillerato</option>
                                            <option value={'Pre Grado'}>Pre Grado</option>
                                            <option value={'Post Grado'}>Post Grado</option>
                                        </select>
                                    </div>
                                    <div>
                                        <select onChange={(e) => { setPaisResidencia(e.target.value) }} value={paisResidencia} required={requerido} >
                                            <option value='' disabled selected>Elige País de Residencia</option>
                                            <option value={'Argentina'}>Argentina</option>
                                            <option value={'Bolivia'}>Bolivia</option>
                                            <option value={'Chile'}>Chile</option>
                                            <option value={'Colombia'}>Colombia</option>
                                            <option value={'Ecuador'}>Ecuador</option>
                                            <option value={'Paraguay'}>Paraguay</option>
                                            <option value={'Perú'}>Perú</option>
                                            <option value={'Uruguay'}>Uruguay</option>
                                            <option value={'Venezuela'}>Venezuela</option>
                                        </select>
                                        <input type='text' onChange={(e) => { setMovil(e.target.value.replace(/\D/g, '')) }} value={movil} placeholder='Número de Contacto' required={requerido} />
                                    </div>
                                    <div>
                                        <input type='text' onChange={(e) => { setCiudadResidencia(e.target.value) }} value={ciudadResidencia} placeholder='Ciudad de Residencia' required={requerido} />
                                        <input type='text' onChange={(e) => { setTelefono(e.target.value.replace(/\D/g, '')) }} value={telefono} placeholder='Teléfono Convencional' required={requerido} />
                                    </div>

                                    <div>
                                        <p> <strong>Credenciales</strong> </p>
                                    </div>
                                    <div>
                                        <input type='email' placeholder='Correo' value={correo} onChange={(e) => { setCorreo(e.target.value) }} required={requerido} />
                                        <input type='text' placeholder='Contraseña' value={pass} onChange={(e) => { setPass(e.target.value) }} required={requerido} />
                                    </div>

                                </div>
                                <div className='Content-register'>
                                    <p><FontAwesomeIcon icon={faPersonDigging} color={'#2A6D75'} /><strong className='title'>Datos Del Emprendimiento</strong></p><br />
                                    <div>
                                        <input onChange={(e) => { setNombreEm(e.target.value) }} value={nombreEm} type='text' placeholder='Nombre' title="Nombre del emprendimiento" required={requerido} />
                                        <select onChange={(e) => { setTiempoEm(e.target.value) }} value={tiempoEm} required={requerido} >
                                            <option value='' disabled selected>Tiempo de funcionamiento</option>
                                            <option value={'menor a 1 año'}>menor a 1 año</option>
                                            <option value={'1 - 4 año'}>1 - 4 año</option>
                                            <option value={'mas de 6 años'}>mas de 6 años</option>
                                        </select>
                                    </div>

                                    <div>
                                        <div className='inputs_empre'>
                                            <input type='text' onChange={(e) => { setCiudaEm(e.target.value) }} value={ciudaEm} placeholder='Ciudad' required={requerido} />
                                            <input type='text' onChange={(e) => { setCantonEm(e.target.value) }} value={cantonEm} placeholder='Cantón' required={requerido} />
                                            <select onChange={(e) => { setEtapaEm(e.target.value) }} value={etapaEm} required={requerido} >
                                                <option value='' disabled selected>Etapa</option>
                                                <option value={'inicial'}>Inicial</option>
                                                <option value={'grande'}>Media</option>
                                            </select>
                                            <input type='text' onChange={(e) => { setNumPersonasEm(e.target.value.replace(/\D/g, '')) }} value={numPersonasEm} placeholder='Número de personas a Cargo' required={requerido} />

                                        </div>
                                        <div className='text_empre'>
                                            <textarea onChange={(e) => { setDescripcionEm(e.target.value) }} value={descripcionEm} placeholder='Descripción' required={requerido} />
                                        </div>

                                    </div>

                                    <div>
                                        <p> <strong>Redes Sociales</strong> </p>
                                    </div>
                                    <div>
                                        <input type='text' placeholder='Linkedin' required={requerido} />
                                        <input type='text' placeholder='Facebook' required={requerido} />
                                    </div>
                                    <div>
                                        <input type='text' placeholder='Twiter' required={requerido} />
                                        <input type='text' placeholder='Instagram' required={requerido} />
                                    </div>

                                </div>
                            </form>

                        </div>
                        <div className='content-buttons' style={{ marginTop: 15 }}>
                            {/* <button className='cancel' onClick={() => { alert('cancelamos'); }}>Cancelar</button> */}
                            <button className='send' form='form-register' type='submit'>Regístrate</button>


                        </div>
                        <Modals type={typeModal} open={openModal} message={messageModal} handleclose={() => { handleCloseModal() }} />


                    </section>
                </>
                : <Loading />}


            <Footer />
        </div >
    )
}