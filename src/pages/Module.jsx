import { useState, useEffect } from 'react';
import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';
import { Loading } from '../components/loading';
import { useNavigate } from 'react-router-dom';
import { getUser } from 'react-session-persist';
import $ from 'jquery';
import '../styles/module.css';
import axios from 'axios';
import { Modals } from '../components/modal';
import fileDownload from 'js-file-download'

import pdfImage from '../images/pdf.svg';

import { Services } from '../constant/services';
export const Module = () => {
    const navigate = useNavigate();

    const [infoModule, setInfoModule] = useState({});
    const [infoDocs, setInfoDocs] = useState({});
    const [question, setQuestions] = useState({});
    const [stateData, setStateData] = useState(1);
    const [loading, setLoading] = useState(false);
    const [loadingQuestion, setLoadingQuestion] = useState(false);
    /* Hocks utilizados para el control del componente Modal*/
    const [openModal, setOpenModal] = useState(false);
    const [messageModal, setMessageModal] = useState('Mensaje predefinido');
    const [typeModal, setTypeModal] = useState(0);

    const [titleBtn, setTitle] = useState("Continual");


    const getModule = async () => {

        var data = JSON.stringify({
            "usuario": {
                "id_registro": getUser().registro
            }
        });
        var url = '/participe/obtenerModulo';
        const responseSession = await Services(data, url);
        if (responseSession.status) {
            setInfoModule(responseSession.data.dataModule);
            setInfoDocs(responseSession.data.dataRecurse);
            setLoading(responseSession.status);
            setTitle(responseSession.data.titleBtn)

        } else {
            alert('error');
            console.log(responseSession)
        }

    }
    const setRequestUser = async () => {
        const data = JSON.stringify({
            "usuario": {
                "id_registro": getUser().registro
            }
        });
        console.log("entramos a nivelar ")
        const url = '/participe/actualizarProgreso';
        const responseSession = await Services(data, url);
        if (responseSession.data.status && responseSession.data.complete) {
            setTypeModal(2)
            setOpenModal(true);
            setMessageModal(responseSession.data.message)
            navigate('/plan')
        } else {
            navigate('/curso')
        }

    }
    const getQuestion = async () => {
        setStateData(0);
        const data = JSON.stringify({
            "data": {
                "modulo": infoModule.id
            }
        });
        const url = '/participe/obtenerPreguntas';
        const responseSession = await Services(data, url);
        if (responseSession.data.tipo == "directo") {
            await setRequestUser();
            return
        }
        if (responseSession.data.data) {

            setLoadingQuestion(responseSession.data.status);
            setQuestions(responseSession.data.data.relaciones);
            $('body,html').animate({ scrollTop: '0px' }, 1000);
        } else {
            setTypeModal(1);
            setOpenModal(true);
            setMessageModal(responseSession.data.message);
        }
    }
    const checkRequest = () => {
        let correctRequest = 0;
        $("input[class='correcto']:checked").each(function (index, check) {
            correctRequest++;
        });
        if (question.length > correctRequest) {
            setTypeModal(0)
            setOpenModal(true);
            setMessageModal('Tiene algunas respuestas fallidas, por favor revise nuevamente el módulo.')
        } else {
            setTypeModal(2);
            setOpenModal(true);
            setMessageModal('!Felicitaciones! Ya has completado este módulo');
            setTimeout(() => {
                setRequestUser();
            }, 2000);
        }
    }

    // !!! handleFile(pdf, "PlanDeNegocios.pdf");
    const handleFile = (url, filename) => {
        try {
            axios.get(url, {
                responseType: 'blob',
            }).then((res) => {
                fileDownload(res.data, filename);
            })
        } catch (error) {
            console.log(error)

        }

    }
    useEffect(() => {
        getModule()
    }, [])
    return (
        <div className="App">
            <Navbar back={stateData === 1 ? () => { navigate('/curso') } : () => { setStateData(1) }} />
            {loading ?
                <>
                    {stateData === 1 ?
                        <section className="Modulo">
                            <div>
                                <p className='position'> <b>{infoModule.posicion}</b> </p>
                                <p className='title'>{infoModule.titulo}</p>
                            </div>
                            <div>
                                <div className='content-img'>
                                    <img src={'data:image/png;base64,' + infoModule.portada} />
                                </div>
                                <p className='description'>{infoModule.descripcion}</p>
                            </div>
                            {infoDocs &&
                                <>
                                    <div className='titleC'>
                                        <p>{infoDocs.titulo}</p>
                                    </div>
                                    <div className='contentVideo'>
                                        <iframe src={infoDocs.video} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                    <div className='contextPdf' onClick={() => { handleFile(infoDocs.documento, `Contenido_Modulo${infoModule.posicion}.pdf`) }}>
                                        <div>
                                            <p className='title2'>Contenido Módulo {infoModule.posicion}</p>
                                            <p className='subtitle'>Descargar</p>
                                        </div>
                                        <div>
                                            <img src={pdfImage} />
                                        </div>
                                    </div>


                                </>

                            }


                            <button onClick={() => { getQuestion() }}>{titleBtn}</button>
                        </section >
                        :
                        <section className="Questions">
                            <div>
                                <p className='title'>Evaluación del módulo: {infoModule.titulo}</p>
                            </div>
                            <div>
                                <p className='info'>En cada pregunta seleccione la respuesta correcta. Cuando haya terminado seleccione "Enviar".</p>
                            </div>

                            {loadingQuestion ?
                                <>
                                    {
                                        question.map(number =>
                                            <div className='content-question'>
                                                <p className='number'><b>{number.posicion}</b></p>
                                                <div>
                                                    <p className='titleQ'>{number.pregunta}</p>
                                                    <br />
                                                    {number.opciones.map(number2 =>
                                                        <label className="container1" id="c1">
                                                            <input type="radio" className={number2.valor} name={number.posicion} />
                                                            <span className="checkmark"></span>
                                                            {number2.descripcion}
                                                        </label>
                                                    )}
                                                    <br />

                                                </div>
                                            </div>

                                        )
                                    }
                                    <button onClick={() => { checkRequest(); }}>Enviar</button>
                                </>

                                :
                                <Loading />
                            }

                        </section >
                    }
                    <Modals type={typeModal} open={openModal} message={messageModal} handleclose={() => { setOpenModal(false); }} />
                </>


                : <Loading />
            }

            <Footer />
        </div >
    )
}