import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';
import { Modules } from '../components/modules';
import { Loading } from '../components/loading';
import { Modals } from '../components/modal';
import { getUser } from 'react-session-persist';
import { Services } from '../constant/services';
import Logo from '../images/planNegocios.png';
import Logo1 from '../images/ClipboardList.svg';
import Logo2 from '../images/CircleQuestion.svg';
import Logo3 from '../images/ListCheck.svg';
import Logo4 from '../images/FileArrow.svg';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import '../styles/plan.css';

export const Plan = () => {
    const navigate = useNavigate();

    const [infoCourse, setInfoCourse] = useState({});
    const [loading, setLoading] = useState(true);
    /* Hocks utilizados para el control del componente Modal*/
    const [openModal, setOpenModal] = useState(false);
    const [messageModal, setMessageModal] = useState('Mensaje predefinido');
    const [typeModal, setTypeModal] = useState(0);
    /**/
    const [preguntas, setPreguntas] = useState(
        [
            {
                titulo: 'Descripción de la Empresa',
                open: false,
                preguntas: [
                    {
                        pregunta: `Enumere las cosas que debe tener para que su empresa alcance el éxito.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Si actualmente tiene una empresa, ¿hace cuánto la administra? Si aún no tiene una empresa, ¿qué medidas ha adoptado para iniciarla?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Cuál es el nombre de su empresa?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Qué idea tiene para su empresa? (Qué productos o servicios ofrecerá, etc.)`,
                        valor: '',
                        open: false
                    },
                ]

            },
            {
                titulo: 'Productos y Servicios',
                open: false,
                preguntas: [
                    {
                        pregunta: `Describa la necesidad que piensa que sus productos o servicios satisfarían`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `De la manera más detallada posible, describa sus productos o servicios actuales`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿En qué se diferenciará su producto o servicio (y será mejor) en relación con los productos o servicios ofrecidos por su competencia?`,
                        valor: '',
                        open: false
                    },
                ]

            },
            {
                titulo: 'Marketing',
                open: false,
                preguntas: [
                    {
                        pregunta: `Enumere las fortalezas y las debilidades de los competidores que ha identificado`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Escriba cómo puede demostrarles a sus clientes que sus productos o servicios son diferentes o mejores que los de la competencia`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Describa cómo evaluó la idea de su empresa con otras personas.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Dónde prefieren comprar sus clientes los tipos de productos o servicios que usted ofrece?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Cuándo desean comprar sus clientes los tipos de productos y servicios que usted ofrece?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Por qué le compran a usted sus clientes? O bien, ¿por qué le comprarán a usted?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Cómo desea que se sientan sus clientes sobre su marca?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Enumere los periódicos, revistas, emisoras de radio y televisión donde realizará la publicidad de su empresa.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Cómo buscará a los vendedores para vender su producto?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Si planea tener un sitio web o usar redes sociales, describa cómo ayudará esto a promocionar su empresa.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Escriba en detalle el proceso que utilizará para fijar los precios para su producto o servicio.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Describa cómo planea mantenerse en contacto con sus clientes existentes.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Describa cómo planea hacer crecer su empresa. ¿Planea ingresar a otros mercados o agregar nuevos productos o servicios, o hacer algo más?`,
                        valor: '',
                        open: false
                    },

                ]

            },
            {
                titulo: 'Plan Operativo',
                open: false,
                preguntas: [
                    {
                        pregunta: `¿Qué tipo de permisos necesitará para administrar su empresa? Haga una 
                        lista de cualquier permiso apropiado que podría necesitar para administrar su empresa.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Describa detalladamente la ubicación de su empresa.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Si tiene una empresa de servicios, ¿cómo y dónde suministrará sus servicios a los clientes?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Anote y describa todos los suministros o materia prima que necesitará para administrar su empresa de manera adecuada.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Describa los trabajos o las tareas para los cuales necesita contratar empleados.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Realice un cálculo del costo de los equipos e instalaciones que necesitará para su empresa.`,
                        valor: '',
                        open: false
                    },
                ]

            },
            {
                titulo: 'Administración',
                open: false,
                preguntas: [
                    {
                        pregunta: `¿Existen roles de gestión/liderazgo que considera que serán necesarios cubrir para iniciar o desarrollar su empresa? Si es así, explique cómo serían esos roles.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Quién le suministrará asesoramiento legal si lo necesita?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Cómo planea compensarse usted mismo?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Quiénes serán los propietarios legítimos de la empresa? ¿Será solo usted o tendrá socios?`,
                        valor: '',
                        open: false
                    },
                ]

            },
            {
                titulo: 'Finanzas',
                open: false,
                preguntas: [
                    {
                        pregunta: `¿Cómo es que manejará el dinero de la empresa?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Cuáles son las fuentes de financiamiento de su empresa? Si las fuentes son personas, anote los nombres. Si la fuente es un banco, anote el nombre del banco y el representante del banco con el que usted trabaja.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Cuánto dinero necesitará para iniciar o expandir su empresa?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Cuál es el costo de fabricación de su producto o prestación de su servicio?`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `¿Cuáles son sus proyecciones de ventas mensuales para los próximos 12 meses? No tiene que ser exacto, pero haga su mejor estimación?`,
                        valor: '',
                        open: false
                    },
                ]

            },
            {
                titulo: 'Plan de Acción',
                open: false,
                preguntas: [
                    {
                        pregunta: `Anote sus objetivos de administración para los primeros tres meses posteriores de completado su plan de negocios.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Describa el apoyo y los recursos de los que dependerá para alcanzar los objetivos que ha definido. Tómese su tiempo y explique esto en detalle.`,
                        valor: '',
                        open: false
                    },
                    {
                        pregunta: `Describa las medidas que usará para determinar la efectividad de su empresa (otras medidas incluyen desperdicios/restos, satisfacción del cliente, entrega puntual, etc.).`,
                        valor: '',
                        open: false
                    },
                ]

            },
        ]
    );

    const verificar = async () => {
        setLoading(true);

        const data = JSON.stringify({
            "usuario": {
                "id_registro": getUser().registro
            }
        });
        const url = '/participe/verificarAvance';
        const responseSession = await Services(data, url);
        if (responseSession.data.status) {
            if (responseSession.data.pdf != null) {
                navigate('/felicidades');
                return
            }
            if (responseSession.data.complete) {
                setLoading(false);
                return
            }
            navigate('/felicidades');
            return;
        } else {
            alert('error')
        }
    }
    const handleTitulo = async (index) => {
        let nuevoPreguntas = preguntas
        let estado = !preguntas[index].open
        nuevoPreguntas.map((item, index2) => {
            preguntas[index2].open = false
        });
        nuevoPreguntas[index].open = estado
        setPreguntas(nuevoPreguntas)
        handleRemoveItems(30)
    }
    const handleSubtitulo = async (index1, index2) => {
        let viejo = preguntas;

        let nuevoPreguntas = preguntas[index1].preguntas
        let estado = !preguntas[index1].preguntas[index2].open
        nuevoPreguntas.map((item, index3) => {
            preguntas[index1].preguntas[index3].open = false
        });
        nuevoPreguntas[index2].open = estado
        viejo[index1].preguntas = nuevoPreguntas;
        setPreguntas(viejo)
        handleRemoveItems(30)
    }

    const handleResponde = async (index1, index2, valor) => {
        let viejo = preguntas;
        viejo[index1].preguntas[index2].valor = valor;
        setPreguntas(viejo)
        handleRemoveItems(30)
    }

    const handleRemoveItems = (id) => {
        const filteredLibraries = preguntas.filter((item) => item.id !== id);
        setPreguntas(filteredLibraries)
    }
    const onsubmit = async () => {
        setLoading(true);
        let alerta = false;
        let mensaje = 'informativo'
        preguntas.map((item, index) => {
            item.preguntas.map((item2, index2) => {
                if (item2.valor.length <= 0 && !alerta) {
                    alerta = true;
                    mensaje = `Responda en el apartado ${item.titulo} todas las preguntas para poder continuar`
                    return (item, item2);
                }
            });
        });
        // return
        if (alerta) {
            setLoading(false);

            setMessageModal(mensaje);
            setOpenModal(true);
            setTypeModal(1)
        } else {
            const data = JSON.stringify({
                "usuario": {
                    "id_registro": getUser().registro
                },
                "datos": preguntas
            });
            const url = '/participe/generarPlan';
            const responseSession = await Services(data, url);
            setLoading(false);

            if (responseSession.data.status) {
                navigate('/felicidades')
            } else {
                alert('error')
            }

        }
    }


    useEffect(() => {
        verificar()
    }, [])

    return (
        <div className="App">
            <Navbar />
            {!loading ?

                <section className="Plan">
                    <Modals />
                    <div className='infoTutor'>
                        <div>
                            <p className='title'>Mi Plan de Negocios</p>
                            <p className='description'>
                                <p className='subtitle'>Elabora un plan de negocios efectivo para tu empresa.</p>
                                Para llevar a cabo un negocio, será útil crear un plan comercial: así podrás desarrollar tu idea al detalle y podrás lograr que realmente es funcional desde un punto de vista logístico y financiero.</p>
                        </div>
                        <div className='tutor-content'>
                            <img src={Logo} />
                        </div>
                    </div>
                    <p className='preguntaP'>¿Cómo empezamos?</p>
                    <div className='listInfo'>
                        <div className='img'>
                            <img src={Logo1} />

                        </div>
                        <div className='text'>
                            Tu plan de negocio estará estructurado por las 7 secciones que se muestran a continuación.
                        </div>
                    </div>

                    <div className='listInfo'>
                        <div className='text'>
                            En cada una de estas secciones se requiere que completes las actividades y preguntas que se presentan.
                        </div>
                        <div className='img'>
                            <img src={Logo3} />

                        </div>

                    </div>
                    <div className='listInfo'>
                        <div className='img'>
                            <img src={Logo2} />

                        </div>
                        <div className='text'>
                            Al completar el plan de negocio será enviado al tutor asignado
                        </div>
                    </div>
                    <div className='listInfo'>

                        <div className='text'>
                            Una vez terminado tu plan de negocios podrás descargarlo
                        </div>
                        <div className='img'>
                            <img src={Logo4} />
                        </div>
                    </div>

                    <div className='contentModule'>
                        <div>
                            {preguntas.map((item, index1) => (
                                <>
                                    <p className='titulo' onClick={() => { handleTitulo(index1) }} >{item.titulo}</p>
                                    {preguntas[index1].open && (
                                        <>
                                            {item.preguntas.map((item2, index2) => (
                                                <>

                                                    <div className='contentPre' >
                                                        <div className='icon' onClick={() => { handleSubtitulo(index1, index2) }} >
                                                            <FontAwesomeIcon icon={item2.open ? faChevronUp : faChevronDown}
                                                                color={'#B525AB'} />
                                                        </div>
                                                        <div className='question' >
                                                            <p onClick={() => { handleSubtitulo(index1, index2) }}> {item2.pregunta}</p>
                                                            {item2.open && (
                                                                <textarea value={preguntas[index1].preguntas[index2].valor}
                                                                    onChange={(e) => { handleResponde(index1, index2, e.target.value) }} ></textarea>
                                                            )}
                                                        </div>

                                                    </div>


                                                </>

                                            ))}

                                        </>
                                    )}
                                </>
                            ))}
                        </div>
                    </div>
                    <div className='contentBTN'>

                        <button className='orangeBtn1' onClick={() => { onsubmit() }}> Enviar </button>

                    </div>
                </section>

                :

                <Loading />

            }


            <Modals type={typeModal} open={openModal} message={messageModal} handleclose={() => { setOpenModal(false) }} />

            <Footer />

        </div >
    )
}