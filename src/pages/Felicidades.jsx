import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';
import { Modules } from '../components/modules';
import { Loading } from '../components/loading';
import { Modals } from '../components/modal';
import { getUser } from 'react-session-persist';
import { Services } from '../constant/services';
import Confetti from 'react-confetti'
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import '../styles/felicidades.css';
import axios from 'axios'
import fileDownload from 'js-file-download'


export const Felicidades = () => {
    const navigate = useNavigate();

    const [infoCourse, setInfoCourse] = useState({});
    const [loading, setLoading] = useState(false);
    /* Hocks utilizados para el control del componente Modal*/
    const [openModal, setOpenModal] = useState(false);
    const [messageModal, setMessageModal] = useState('Mensaje predefinido');
    const [typeModal, setTypeModal] = useState(0);
    const [pdf, setPdf] = useState(null);
    const [ancho, setAncho] = useState(null);
    const [largo, setLargo] = useState(null);



    /**/


    const obtenerplan = async () => {
        setAncho(window.screen.height)
        setLargo(window.screen.width)

        setLoading(true);

        const data = JSON.stringify({
            "usuario": {
                "id_registro": getUser().registro
            }
        });
        const url = '/participe/verificarAvance';

        const responseSession = await Services(data, url);
        if (responseSession.data.status) {
            // console.log(responseSession.data.infoPdf)

            if (responseSession.data.pdf != null) {
                setLoading(false);
                setPdf(responseSession.data.pdf)
                return
            }
            navigate('/curso');
        } else {
            alert('error')
        }
    }
    const handleFile = (url, filename) => {
        try {
            axios.get(url, {
                responseType: 'blob',
            }).then((res) => {
                fileDownload(res.data, filename)
            })
        } catch (error) {
            console.log(error)

        }

    }
    const onsubmit = async () => {
        handleFile(pdf, "PlanDeNegocios.pdf");
        return;
    }



    useEffect(() => {
        obtenerplan()
    }, [])

    return (
        <div className='App' >
            <Navbar />
            {!loading ?

                <section className="Felicidades">
                    <Confetti width={largo} height={ancho} />

                    <div className='info'>
                        <p className='title'>!FELICITACIONES!</p>
                        <p className='subtitle'>Has finalizado con éxito tu capacitación empresarial en Innova-T</p>
                        <div className='contentBTN'>
                            <button className='orangeBtn1' onClick={() => { onsubmit() }}> Descargar Plan de Negocios </button>
                        </div>
                    </div>
                </section>

                :

                <Loading />

            }


            <Modals type={typeModal} open={openModal} message={messageModal} handleclose={() => { setOpenModal(false) }} />

            <Footer />

        </div >
    )
}