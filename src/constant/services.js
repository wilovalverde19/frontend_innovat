import axios from 'axios';
import { decryptText } from './encripter';


export const Services = async (data = {}, url) => {
    var config = {
        method: 'post',
        url: `${process.env.REACT_APP_BACK + url}`,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer <token_here>'
        },
        data: data
    };
    const request = await axios(config)
        .then(function (response) {
            // console.log(response.data.data)
            // decryptText(response.data.data, response.data.info)
            return response;
        })
        .catch(function (error) {
            return error
        });

    return request
}