/*
    APIS[0] = Api para obtener todas 
    APIS[1] = Api para obtener datos 

*/


export const APIS = [
    {
        name: "http://169.62.234.126:3000/entity",
        //name: "http://localhost:3000/entity",
        metodo: "POST",
        descripcion: "Api para obtener caracteristicas de la Entidad Financiera"
    },
    {
        //name: "https://www.fintech.kradac.com:3006/civil_registry/validate",
        name: "http://169.62.234.126:3000/civil_registry/validateC",
        metodo: "POST",
        descripcion: "Api para obtener datos del registro civil"
    },
    {
        //name: "https://www.fintech.kradac.com:3006/recognition/comparate",
        name: "http://localhost:3000/recognition/comparate",
        metodo: "POST",
        descripcion: "Api para obtener comparacion de la cedula y selfie"
    },
]