FROM node:16-alpine

WORKDIR /app

COPY package*.json .

RUN npm install --force
# RUN npm ci --only=production

COPY . .

RUN npm run build

EXPOSE 5001

CMD ["npm", "start"]
